import java.util.*;

class InvalidTurnException extends RuntimeException {
  InvalidTurnException(String message) { super(message); }
}

class GameState implements GameEventListener {
  private Deck deck;
  private List<Discard> pile;
  private Map<String, Set<Card>> playerHands;

  GameState(Deck deck, List<Discard> pile, Map<String, Set<Card>> playerHands) {
    this.deck = deck;
    this.pile = pile;
    this.playerHands = playerHands;
  }

  GameState(Deck deck) {
    this.deck = deck;
    this.pile = new ArrayList<>();
    Discard initialDiscard;
    Card firstCard = this.deck.drawRandomCard();

    // if the first card is a joker we will allow it to represent EVERYTHING!
    if(firstCard.getValue() == Value.JOKER) {
      initialDiscard = Discard.getInitialJokerDiscard(firstCard);
    } else {
      initialDiscard = new Discard.Builder()
          .addCard(firstCard)
          .build();
    }

    this.pile.add(initialDiscard);
    this.playerHands = new HashMap<>();
  }

  void deal(List<Player> players) {
    for(Player player : players) {
      playerHands.put(player.getName(), new TreeSet<>());
    }

    for(int i = 0; i < 5; i++) {
      for(Player player : players) {
        Set<Card> hand = playerHands.get(player.getName());
        hand.add(deck.randomlyDealCard());
        playerHands.put(player.getName(), hand);
      }
    }

/*
    for(Player player : players) {
      if (player.getName() == "Killer") {
        Set<Card> hand = new TreeSet<>();
        hand.add(new Card(Value.JOKER, Suit.JOKER_1));
        hand.add(new Card(Value.TWO, Suit.CLUBS));
        hand.add(new Card(Value.TWO, Suit.SPADES));
        hand.add(new Card(Value.TWO, Suit.DIAMONDS));
        hand.add(new Card(Value.ACE, Suit.CLUBS));
        playerHands.put(player.getName(), hand);
      } else {
        Set<Card> hand = new TreeSet<>();
        hand.add(new Card(Value.JOKER, Suit.JOKER_2));
        hand.add(new Card(Value.TWO, Suit.HEARTS));
        hand.add(new Card(Value.ACE, Suit.DIAMONDS));
        hand.add(new Card(Value.ACE, Suit.HEARTS));
        hand.add(new Card(Value.ACE, Suit.SPADES));
        playerHands.put(player.getName(), hand);
      }
    }*/
  }

  void resetHand(Player player) {
    playerHands.put(player.getName(), new TreeSet<>());
  }

  int getValueOfPlayersHand(Player player) {
    return getValueOfPlayersHand(player.getName());
  }

  int getValueOfPlayersHand(String player) {
    Set<Card> hand = playerHands.get(player);
    int result = 0;
    for (Card card : hand) {
      result += card.getNumericValue();
    }
    return result;
  }

  Set<Card> getCardsInPlayersHand(Player player) {
    return getCardsInPlayersHand(player.getName());
  }

  Set<Card> getCardsInPlayersHand(String player) {
    return Collections.unmodifiableSet(playerHands.get(player));
  }

  int getDeckSize() {
    return deck.cards.size();
  }

  void setDeck(Deck deck) {
    this.deck = deck;
  }

  Discard getLastDiscard() {
    if (pile.size() > 0) {
      return pile.get(pile.size()-1);
    } else {
      throw new IllegalStateException("There should always be a previous discard.");
    }
  }

  private void setLastDiscard(Discard discard) {
    pile.add(discard);
  }

  public void onGameEvent(GameEvent event) {
    if (event.getType() == GameEvent.EventType.DISCARDED) {
      String player = event.getPlayer();
      Card pickedUp = event.getPickedUp();
      Discard discard = event.getDiscard();
      Set<Card> hand = playerHands.get(player);
      if (!playerDiscardedCorrectCardForJoker(pickedUp, discard, getLastDiscard())) {
        throw new InvalidTurnException("Invalid turn: You discarded something that the joker didn't represent but took the joker anyway.");
      }
      if (!playerDrewFromTheEdge(pickedUp, getLastDiscard())) {
        throw new InvalidTurnException("Invalid turn: You need to draw from the edge of the discard.");
      }
      if (!getCardsInPlayersHand(player).containsAll(discard.getCards())) {
        throw new InvalidTurnException("Invalid turn: You discarded something that you didn't have.");
      }
      if (pickedUp == null || (pickedUp.getValue() == Value.UNKNOWN && pickedUp.getSuit() == Suit.UNKNOWN)) {
        hand.add(deck.randomlyDealCard());
      } else if (!getLastDiscard().getCards().contains(pickedUp)) {
        throw new InvalidTurnException("Invalid turn: You tried to pick up something that wasn't in the last discard.");
      } else {
        // Valid discard
        hand.add(pickedUp);
      }
      //  isValidDiscard(event.getPlayer(), discard);
      hand.removeAll(discard.getCards());
      playerHands.put(player, hand);
      this.setLastDiscard(discard);
    }
  }

  private void isValidDiscard(Player player, Set<Card> discard) {
    Set<Card> hand = playerHands.get(player);
  }

  static boolean playerDiscardedCorrectCardForJoker(Card pickedUp, Discard discard, Discard lastDiscard) {
    // player didn't pick up a joker so we don't have any problems.
    if (pickedUp == null || pickedUp.getValue() != Value.JOKER) {
      return true;
    }
    if (discard.getCards().size() != 1) {
      return false;
    }
    return lastDiscard.getJokerValues().contains(
        discard.getCards().iterator().next());
  }

  static boolean playerDrewFromTheEdge(final Card pickedUp, final Discard lastDiscard) {
    if (pickedUp == null ||
        pickedUp.getValue() == Value.UNKNOWN ||
        pickedUp.getValue() == Value.JOKER) {
      return true;
    }

    // if there are fewer than three cards there's no way to pick an invalid card.
    if (lastDiscard.getCards().size() < 3) {
      return true;
    }

    SortedSet<Card> effectiveRun = new TreeSet<>(lastDiscard.getCards());
    effectiveRun.addAll(lastDiscard.getJokerValues());
    effectiveRun.removeAll(Card.JOKERS);

    // We only care if this is a run which is why we check if the values of the first
    // and last card in the set are the same value.
    if (!pickedUp.equals(effectiveRun.first()) && !pickedUp.equals(effectiveRun.last()) &&
        effectiveRun.first().getValue() != effectiveRun.last().getValue()) {
      return false;
    }
    return true;
  }
}
