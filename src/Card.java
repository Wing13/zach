import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

// The order of these enums defines their order for the purpose of determining straights.
// Don't move them around on sorting the cards won't work.
enum Value {
  ACE(1),
  TWO(2),
  THREE(3),
  FOUR(4),
  FIVE(5),
  SIX(6),
  SEVEN(7),
  EIGHT(8),
  NINE(9),
  TEN(10),
  JACK(10),
  QUEEN(10),
  KING(10),
  JOKER(0),     // These last two cases are a little strange
  UNKNOWN(null);  // Joker doesn't have a suit, Unknown doesn't have a suit or a value.

  private final Integer value;

  Value(Integer value) {
    this.value = value;
  }

  public Integer getValue() {
    return this.value;
  }

  public static Value fromString(String valueString) {
    if (valueString == null) {
      throw new IllegalArgumentException("String does not match any Value.");
    }
    for (Value value : Value.values()) {
      if (value.name().equalsIgnoreCase(valueString)) {
        return value;
      }
    }
    throw new IllegalArgumentException("String does not match any Value.");
  }
}

enum Suit {
  CLUBS,
  DIAMONDS,
  HEARTS,
  SPADES,
  JOKER_1,
  JOKER_2,
  UNKNOWN;

  public static Suit fromString(String suitString) {
    if (suitString == null) {
      throw new IllegalArgumentException("String does not match any Suit.");
    }
    for (Suit suit : Suit.values()) {
      if (suit.name().equalsIgnoreCase(suitString)) {
        return suit;
      }
    }
    throw new IllegalArgumentException("String does not match any Suit.");
  }
}

class Card implements Comparable<Card> {
  private final Value value;
  private final Suit suit;

  Card(Value value, Suit suit) {
    if (value == Value.UNKNOWN ^ suit == Suit.UNKNOWN) {
      throw new IllegalArgumentException("Both or neither of Value and Suit must be UNKNOWN. " +
          "You specified a Value of " + value.name() +
          " and a Suit of " + suit.name());
    }

    if (value == Value.JOKER ^ (suit == Suit.JOKER_1 || suit == Suit.JOKER_2)) {
      throw new IllegalArgumentException("A joker must have a Value of JOKER and Suit of " +
          "either JOKER_1 or JOKER_2. You specified a Value of " + value.name() +
          " and a Suit of " + suit.name());
    }

    this.value = value;
    this.suit = suit;
  }

  Card(String value, String suit) {
    if (value == null || suit == null) {
      throw new IllegalArgumentException("Please specify valid strings for both value and suit.");
    }

    this.value = Value.fromString(value);
    this.suit = Suit.fromString(suit);
  }

  Value getValue() {
    return this.value;
  }

  Suit getSuit() {
    return this.suit;
  }

  int getNumericValue() {
    return this.value.getValue();
  }

  public static int getValueOfCards(Set<Card> cards) {
    int sum = 0;
    for (Card card : cards) {
      sum += card.getNumericValue();
    }
    return sum;
  }

  public static boolean isRun(Set<Card> cards) {
    if (cards == null || cards.size() < 3 || cards.size() > 13) {
      return false;
    }
    Card previousCard = null;
    for (Card card : cards) {
      if (previousCard != null) {
        if (card.getSuit() != previousCard.getSuit() ||
            card.getValue().ordinal() - previousCard.getValue().ordinal() != 1) {
          return false;
        }
      }
      previousCard = card;
    }
    return true;
  }

  public static boolean isMultiple(Set<Card> cards) {
    if (cards == null || cards.size() < 2 || cards.size() > 4) {
      return false;
    }
    Value previousValue = null;
    for (Card card : cards) {
      if (previousValue != null && card.getValue() != previousValue) {
        return false;
      }
      previousValue = card.getValue();
    }
    return true;
  }

  static Set<Card> getAllCardsOfValue(Value value) {
    Set<Card> result = new TreeSet<>();
    if (value == null) {
      throw new IllegalArgumentException("Please supply a non-null value.");
    }
    if (value == Value.UNKNOWN) {
      throw new IllegalArgumentException("Please supply an known value.");
    }
    if (value == Value.JOKER) {
      result.add(new Card(value, Suit.JOKER_1));
      result.add(new Card(value, Suit.JOKER_2));
      return result;
    }
    result.add(new Card(value, Suit.DIAMONDS));
    result.add(new Card(value, Suit.HEARTS));
    result.add(new Card(value, Suit.CLUBS));
    result.add(new Card(value, Suit.SPADES));
    return result;
  }

  @Override
  public String toString() {
    if(this.value == Value.UNKNOWN || this.suit == Suit.UNKNOWN) {
      return "UNKNOWN";
    } else if (this.value == Value.JOKER) {
      return "JOKER";
    } else {
      return this.value.name() + " of " + this.suit.name();
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof Card)) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    Card otherCard = (Card) obj;

    return otherCard.getValue() == this.getValue() && otherCard.getSuit() == this.getSuit();
  }

  @Override
  public int hashCode() {
    // this returns a unique value for every Card.
    return Value.values().length * this.getSuit().ordinal() + 1 + this.getValue().ordinal();
  }

  @Override
  public int compareTo(Card o) {
    if (o.getValue() == Value.UNKNOWN || this.getValue() == Value.UNKNOWN) {
      throw new IllegalArgumentException("Comparison with unknown cards is undefined.");
    }
    if (this.getNumericValue() == o.getNumericValue()) {
      if (this.getValue().ordinal() == o.getValue().ordinal()) {
        return this.getSuit().ordinal() - o.getSuit().ordinal();
      }
      return this.getValue().ordinal() - o.getValue().ordinal();
    }
    return this.getNumericValue() - o.getNumericValue();
  }

  public static final Set<Card> JOKERS;
  static {
    Set<Card> set = new TreeSet<>();
    set.add(new Card(Value.JOKER, Suit.JOKER_1));
    set.add(new Card(Value.JOKER, Suit.JOKER_2));
    JOKERS = Collections.unmodifiableSet(set);
  }
}
