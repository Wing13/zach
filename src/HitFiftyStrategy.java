import java.util.*;
import java.util.logging.Logger;

// Try to hit 50 to reduce your score.
class HitFiftyStrategy extends Strategy {

  private KillerPlayer parent;
  private int goal;
  private static final Logger log = Logger.getLogger( HitFiftyStrategy.class.getName() );

  @Override
  int estimateReturnOnStrategy(Set<Card> hand) {
    if (Card.getValueOfCards(hand) >= 29 && Card.getValueOfCards(hand) <= 33 &&
        parent.playerScoreView.get(parent.getName()) % 50 >= 28 &&
        parent.playerScoreView.get(parent.getName()) % 50 <= 30) {
      return 5;
    } else {
      return 20;
    }
  }

  @Override
  void setParent(KillerPlayer parent) {
    this.parent = parent;
  }

  @Override
  GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {
    log.fine("One of Killer's submonsters reporting for duty.");
    log.fine("This is my hand:");
    for(Card card : hand) {
      log.fine("  " + card.toString());
    }

    log.fine("These are my available pickups:");
    List<Card> possiblePickups = CommonUtils.getPossiblePickups(lastDiscard);
    for (Card card : possiblePickups) {
      log.fine("  " + card.toString());
    }

    SortedSet<Card> discardCards = new TreeSet<>(hand);
    if (hand.size() > getOptimalNumberOfCardsForGoal(getRoundGoal())) {
      int largestReduction = 0;
      Card largestReductionCard = null;
      List<SortedSet<Card>> bestPossibleNewMultiCardDiscards = null;
      for (Card card : possiblePickups) {
        SortedSet<Card> possibleNewHand = new TreeSet<>(hand);
        possibleNewHand.add(card);

        List<SortedSet<Card>> possibleNewMultiCardDiscards = new ArrayList<>();
        possibleNewMultiCardDiscards.addAll(CommonUtils.detectMultiples(possibleNewHand));
        possibleNewMultiCardDiscards.addAll(CommonUtils.detectRuns(possibleNewHand));

        List<SortedSet<Card>> trimmedNewMultiCardDiscards = new ArrayList<>();
        for (SortedSet<Card> cards : possibleNewMultiCardDiscards) {
          if (cards.contains(card)) {
            trimmedNewMultiCardDiscards.add(cards);
          }
        }

        log.fine("These are the new multiples I could have if I drew the: " + card.toString());
        for (SortedSet<Card> discard : trimmedNewMultiCardDiscards) {
          for (Card newDiscardCard : discard) {
            log.fine("  " + newDiscardCard.toString());
          }
        }

        if (largestReductionCard == null) {
          largestReductionCard = card;
          bestPossibleNewMultiCardDiscards = trimmedNewMultiCardDiscards;
        }
        for (SortedSet<Card> multiCardDiscard : trimmedNewMultiCardDiscards) {
          int reduction = 0;
          for (Card multicard : multiCardDiscard) {
            reduction += multicard.getNumericValue();
          }
          reduction -= card.getNumericValue();
          if (reduction > largestReduction) {
            largestReduction = reduction;
            largestReductionCard = card;
            bestPossibleNewMultiCardDiscards = trimmedNewMultiCardDiscards;
          }
        }
      }

      if (largestReductionCard != null) {
        log.fine("I think the best card for me would be the" + largestReductionCard.toString());
      }

      SortedSet<Card> trimmedHand = new TreeSet<>();
      while (trimmedHand.isEmpty()) {
        trimmedHand.addAll(hand);
        if (bestPossibleNewMultiCardDiscards != null) {
          bestPossibleNewMultiCardDiscards.forEach(trimmedHand::removeAll);
        }

        log.fine("These are the cards that I have after trimming:");
        for (Card card : trimmedHand) {
          log.fine("  " + card.toString());
        }

        if (trimmedHand.isEmpty()) {
          int smallestValue = 60;
          SortedSet<Card> worstMultiple = null;
          assert bestPossibleNewMultiCardDiscards != null;
          for (SortedSet<Card> multiple : bestPossibleNewMultiCardDiscards) {
            int value = 0;
            for (Card card : multiple) {
              value += card.getNumericValue();
            }
            if (worstMultiple == null || value < smallestValue) {
              smallestValue = value;
              worstMultiple = multiple;
            }
          }

          boolean removedMultiple = bestPossibleNewMultiCardDiscards.remove(worstMultiple);
          assert removedMultiple;
        }
      }
      discardCards = trimmedHand;
      if (bestPossibleNewMultiCardDiscards != null && bestPossibleNewMultiCardDiscards.size() > 0) {
        possiblePickups = new ArrayList<>();
        possiblePickups.add(largestReductionCard);
      } else {
        possiblePickups.add(new Card(Value.UNKNOWN, Suit.UNKNOWN));
      }
    }

    List<SortedSet<Card>> possibleDiscards = CommonUtils.getAllPossibleDiscards(discardCards);

    Map<Integer, List<SortedSet<Card>>> applicableValues =
        CommonUtils.getApplicablePickupValues(possibleDiscards, getRoundGoalDelta(hand));

    double maxProbability = 0;
    int bestValue = -1;
    for (Integer value : applicableValues.keySet()) {
      if (parent.probabilityOfValue(value) > maxProbability) {
        maxProbability = parent.probabilityOfValue(value);
        bestValue = value;
      }
    }

    log.fine("These are my available discards:");
    for (SortedSet<Card> discard : possibleDiscards) {
      for (Card card : discard) {
        log.fine("  " + card.toString());
      }
    }

    log.fine("My goal is: " + String.valueOf(getRoundGoal()));
    log.fine("I currently have: " + String.valueOf(Card.getValueOfCards(hand)));
    log.fine("So I need: " + String.valueOf(getRoundGoalDelta(hand)));
    double smallestDifference = 50;
    SortedSet<Card> bestDiscard;
    boolean strikingDistance;
    if (!applicableValues.isEmpty() && bestValue >= 0) {
      strikingDistance = true;
      bestDiscard = applicableValues.get(bestValue).get(0);
    } else {
      strikingDistance = false;
      bestDiscard = possibleDiscards.get(0);
    }
    Card bestPickup = null;
    for(SortedSet<Card> discard : possibleDiscards) {
      for(Card pickup : possiblePickups) {
        log.fine("Evaluating pickup: " + pickup.toString() + " and discard: " + discard.toString());
        double handChange;
        if (pickup.getValue() != Value.UNKNOWN) {
          handChange = pickup.getNumericValue() - Card.getValueOfCards(discard);
        } else {
          handChange = parent.estimateValueOfUnknownCard() - Card.getValueOfCards(discard);
        }
        double futureDistanceFromGoal = Math.abs(handChange - getRoundGoalDelta(hand));
        log.fine("This will change my score by: " + String.valueOf(handChange));
        log.fine("At which point I'll be this many points away from my goal: " + String.valueOf(futureDistanceFromGoal));

        // constructing the future hand in this case
        double possibleMaxProbability = 0;
        if (pickup.getValue() != Value.UNKNOWN) {
          SortedSet<Card> newHand = new TreeSet<>(hand);
          newHand.removeAll(discard);
          newHand.add(pickup);

          List<SortedSet<Card>> futurePossibleDiscards = CommonUtils.getAllPossibleDiscards(newHand);
          Map<Integer, List<SortedSet<Card>>> futureApplicableValues =
              CommonUtils.getApplicablePickupValues(futurePossibleDiscards, getRoundGoalDelta(hand));

          for (Integer value : futureApplicableValues.keySet()) {
            possibleMaxProbability = Math.max(possibleMaxProbability, parent.probabilityOfValue(value));
          }
        }

        if (futureDistanceFromGoal == 0 && strikingDistance) {
          bestDiscard = discard;
          bestPickup = pickup;
          smallestDifference = 0;
        } else if (!strikingDistance && futureDistanceFromGoal < smallestDifference) {
          bestDiscard = discard;
          bestPickup = pickup;
          smallestDifference = futureDistanceFromGoal;
        } else if (smallestDifference != 0 && possibleMaxProbability > maxProbability) {
          bestDiscard = discard;
          bestPickup = pickup;
        }
      }
    }

    GameEvent.Builder event = new GameEvent.Builder()
        .setType(GameEvent.EventType.DISCARDED)
        .setPlayer(parent);

    event.setPickedUp(bestPickup);

    List<Card> jokerValues;
    if (bestDiscard != null && bestDiscard.size() >= 3) {
      jokerValues = CommonUtils.detectRun(new ArrayList<>(bestDiscard)).jokerValues;
    } else {
      jokerValues = new ArrayList<>(Deck.getFullDeck().cards);
      jokerValues.removeAll(Card.JOKERS);
    }

    Discard.Builder discard = new Discard.Builder();
    int jokerIndex = 0;
    for (Card card : bestDiscard) {
      if (card.getValue() == Value.JOKER) {
        discard.addJoker(card, jokerValues.get(jokerIndex));
        jokerIndex++;
      } else {
        discard.addCard(card);
      }
    }

    event.setDiscard(discard.build());

    return event.build();
  }

  @Override
  public void onGameEvent(GameEvent event) {
  }

  private int getRoundGoal() {
    return 50 - (parent.playerScoreView.get(parent.getName()) % 50);
  }

  private int getRoundGoalDelta(Set<Card> hand) {
    return getRoundGoal() - Card.getValueOfCards(hand);
  }

  static private int getOptimalNumberOfCardsForGoal(int goal) {
    if (goal < 0) {
      throw new IllegalArgumentException("can't have a goal of less than 0");
    }
    if (goal > 50) {
      throw new IllegalArgumentException("Impossible!");
    }
    if (goal == 0) {
      return 1;
    }
    return (goal + 9)/10;
  }
}