import java.util.*;

import java.io.IOException;

final class HumanPlayer extends Player {
  HumanPlayer(String name) {
    this.name = name;
  }
  
  private int getIntFromStdIn() {
    return new Scanner(System.in).nextInt();
  }
  
  private List<Integer> getIntListFromStdIn() {
    String line = new Scanner(System.in).nextLine();

    Scanner sc = new Scanner(line).useDelimiter("\\s*,\\s*");
    List<Integer> response = new ArrayList<>();
    while(sc.hasNextInt()) {
      response.add(sc.nextInt());
    }

    return response;
  }

  private Card getCardFromStdIn() {
    String line = new Scanner(System.in).nextLine();

    Scanner s = new Scanner(line).useDelimiter("\\s*of\\s*");
    String valueName = s.next();
    String suitName = s.next();
    return new Card(valueName, suitName);
  }

  private char getCharFromStdIn() {
    return new Scanner(System.in).next().charAt(0);
  }
  
  GameEvent takeTurn(final Set<Card> hand, Discard lastDiscard) {
    GameEvent turn;
    do {
      turn = getTurnFromHuman(hand, lastDiscard);
      System.out.println("This is your proposed turn:");
      System.out.println(turn.prettyPrint());
      System.out.print("\nIs this the move you'd like to make? (y/n):");

      // confirm with the human, for they are angered easily.
      char response;
      do {
        response = getCharFromStdIn();
        if (Character.toLowerCase(response) != 'y') {
          turn = null;
        }
      } while (response == ' ');
    } while (turn == null);
    return turn;
  }

  private GameEvent getTurnFromHuman(final Set<Card> hand, Discard lastDiscard) {
    GameEvent.Builder builder = new GameEvent.Builder();
    builder.setPlayer(this);

    System.out.println("These are the cards you have in your hand:");
    for (Card card : hand) {
      System.out.println("  " + card.toString());
    }
    System.out.println();

    System.out.println("This is the last discard:");
    for (Card card : lastDiscard.getCards()) {
      System.out.println("  " + card.toString());
    }
    System.out.println();

    GameEvent.EventType type = null;

    if (Card.getValueOfCards(hand) > 7) {
      type = GameEvent.EventType.DISCARDED;
      System.out.println("Value of hand is greater than 7, assuming you don't want to call Zach.\n");
    }

    while(type == null) {
      System.out.println("What would you like to do?");
      System.out.println("  1. Take turn.");
      System.out.println("  2. Call Zach.");

      try {
        int index = this.getIntFromStdIn();
        if (index == 1) {
          type = GameEvent.EventType.DISCARDED;
        } else if (index == 2) {
          type = GameEvent.EventType.CALLED_ZACH;
        } else {
          System.out.println("Please enter 1 or 2.");
        }
      } catch (NumberFormatException e) {
        System.out.println("Your input was badly formatted. Please just enter the number of your selection.");
        System.out.println(e.getMessage());
      }
    }

    builder.setType(type);
    if (type == GameEvent.EventType.CALLED_ZACH) {
      return builder.build();
    } else if (type != GameEvent.EventType.DISCARDED) {
      throw new UnsupportedOperationException("Only calling zach and discarding are supported right now.");
    }

    System.out.println("Enter the comma separated list of cards that you'd like to discard");

    Iterator<Card> handIterator = hand.iterator();
    int counter = 1;
    while (handIterator.hasNext()) {
      StringBuilder sb = new StringBuilder("  ");
      sb.append(counter++);
      sb.append(". ");
      sb.append(handIterator.next().toString());
      System.out.println(sb.toString());
    }

    Discard.Builder discard = null;
    while(discard == null) {
      discard = new Discard.Builder();
      try {
        for (int index : this.getIntListFromStdIn()) {
          index--;
          if (index >= hand.size() || index < 0) {
            System.out.println("Please enter a number within the valid range.");
            discard = null;
            continue;
          }
          handIterator = hand.iterator();
          counter = 0;
          while (counter < index) {
            counter++;
            handIterator.next();
          }
          Card card = handIterator.next();
          if (card.getValue() == Value.JOKER) {
            Card representation = null;
            System.out.println("Please enter which card the joker should represent.");
            while (representation == null) {
              try {
                representation = getCardFromStdIn();
              } catch (IllegalArgumentException e){
                System.out.println("Please enter your card in the format 'VALUE of SUIT'.");
              }
            }
            discard.addJoker(card, representation);
          } else {
            discard.addCard(card);
          }
        }
      } catch (NumberFormatException e) {
        System.out.println("Please enter your selection in the form \"1,2,3\"");
        discard = null;
      }
    }
    builder.setDiscard(discard.build());

    System.out.println("Enter the number of the card that you'd like to pick up.");

    Iterator<Card> discardIterator = lastDiscard.getCards().iterator();
    counter = 1;
    while (discardIterator.hasNext()) {
      StringBuilder sb = new StringBuilder("  ");
      sb.append(counter++);
      sb.append(". ");
      sb.append(discardIterator.next().toString());
      System.out.println(sb.toString());
    }

    String sb = "  " + (lastDiscard.getCards().size() + 1) +
        ". " +
        "A random card from the top of the deck.";
    System.out.println(sb);

    Card card = null;
    while(card == null) {
      try {
        // getIntFromStdIn returns us a 1-indexed array index so we convert.
        int index = this.getIntFromStdIn() - 1;
        if (index < 0 || index > lastDiscard.getCards().size()) {
          System.out.println("Please enter a number within the valid range.");
          continue;
        }
        if (index == lastDiscard.getCards().size()) {
          card = new Card(Value.UNKNOWN, Suit.UNKNOWN);
        } else {
          discardIterator = lastDiscard.getCards().iterator();
          counter = 0;
          while (counter < index) {
            counter++;
            discardIterator.next();
          }
          card = discardIterator.next();
        }
      } catch (NumberFormatException e) {
        System.out.println("Please enter a valid number.");
      }
    }

    if (card.getValue() != Value.UNKNOWN) {
      builder.setPickedUp(card);
    }

    return builder.build();
  }
  
  public void onGameEvent(GameEvent event) {}
}
