import java.util.Map.Entry;

class LoggingEventListener implements GameEventListener {

  String zachCaller;

  public void onGameEvent(GameEvent event) {
    switch(event.getType()) {
      case CALLED_ZACH:
        System.out.println(event.getPlayer() + " called Zach! That's a bold strategy, Cotton. Let's see if it pays off for 'em.");
        zachCaller = event.getPlayer();
        break;

      case DISCARDED:
        // This section describes their discard.
        StringBuilder messageBuilder = new StringBuilder(event.getPlayer() + " discarded:\n");
        for (Card card : event.getDiscard().getCards()) {
          messageBuilder.append(card.toString()).append("\n");
        }
        System.out.println(messageBuilder.toString());

        messageBuilder = new StringBuilder("They picked up:\n");

        if(event.getPickedUp() != null) {
          messageBuilder.append(event.getPickedUp().toString()).append(".");
        } else {
          messageBuilder.append("a random card from the deck.");
        }
        System.out.println(messageBuilder.toString());
        System.out.println("------------");
        break;

      case DECK_REFRESH:
        System.out.println("The deck was refreshed!");
        break;

      case GAME_START:
        System.out.println("Welcome to the show, Folks. It should be a great game tonight. " +
            "Now, to introduce our players.");

        for (int i = 0; i < event.getPlayers().size(); i++) {
          if(i == 0) {
            System.out.println("First up, we have " + event.getPlayers().get(i));
          } else if (event.getPlayers().size() == 2) {
            System.out.println(" and their opponent will be " + event.getPlayers().get(i));
          } else if (i == event.getPlayers().size() - 1) {
            System.out.println("Last but not least, we have " + event.getPlayers().get(i));
          } else {
            System.out.println("Next up, we have " + event.getPlayers().get(i));
          }
        }
        break;

      case ROUND_START:
        System.out.println("Starting this round we have " + event.getStartingPlayer());
        System.out.println("The initial card is " + event.getInitialDiscard().getCards().iterator().next());
        break;

      case ROUND_END:
        if (zachCaller == null) {
          throw new AssertionError("ROUND_END should never happen before ZACH_CALLED");
        }
        if (!zachCaller.equals(event.getWinningPlayer())) {
          System.out.println(zachCaller + " got wing'd!");
        }
        zachCaller = null;
        System.out.println("The winner of the round is " + event.getWinningPlayer());
        System.out.println();

        System.out.println("Round Scores:");
        for (Entry<String, Integer> score : event.getRoundScores().entrySet()) {
          System.out.println(score.getKey() + " : " + score.getValue());
        }
        System.out.println();

        System.out.println("Current Standings:");
        for (Entry<String, Integer> score : event.getGameScores().entrySet()) {
          System.out.println(score.getKey() + " : " + score.getValue());
        }
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------");
        break;

      case GAME_END:
        System.out.println("And that's the ball game! Our champion is " + event.getWinningPlayer()
            + ".");
        break;
    }
  }
}
