import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class ZachGame {
  private List<Player> players;
  private Map<String, Integer> gameScores;
  private List<GameEventListener> gameEventListeners;
  private GuiEventListener gui;
  private Player roundWinner;
  private static final int LOSING_SCORE = 200;
  private static final int ZACH_THRESHOLD = 7;
  private static final int PENALTY = 30;
  private static final int MULTIPLE_OF_FIFTY_REDUCTION = -50;

  ZachGame(List<Player> players) {
    this(players, null);
  }

  ZachGame(List<Player> players, GuiEventListener gui) {
    this.players = players;
    this.gameScores = new HashMap<>();

    // initialize the scores and give each player a view on the scores.
    for (Player player : players) {
      gameScores.put(player.getName(), 0);
      player.setPlayerScoreView(Collections.unmodifiableMap(this.gameScores));
    }

    if (gameScores.size() != players.size()) {
      throw new IllegalArgumentException("Each player must have a unique name.");
    }

    this.gameEventListeners = new ArrayList<>(players);
    this.gameEventListeners.add(new LoggingEventListener());
    this.gui = gui;
    if (this.gui != null) {
      this.gameEventListeners.add(gui);
      this.gui.setPlayers(Collections.unmodifiableList(this.players));
    }
  }

  void playGame() throws Exception {
    if (this.gui != null) {
      this.gui.setVisible(true);
    }
    this.roundWinner = null;

    GameEvent.Builder gameStartEvent = new GameEvent.Builder();
    gameStartEvent.setType(GameEvent.EventType.GAME_START);
    gameStartEvent.setPlayers(getPlayerNames());
    broadcastEvent(gameStartEvent.build());
    while (true) {
      playRound();
      boolean lost = false;
      Integer winningScore = null;
      Player winner = null;
      for (Player player : players) {
        if (gameScores.get(player.getName()) > LOSING_SCORE) {
          lost = true;
        }
        if (winningScore == null || gameScores.get(player.getName()) < winningScore) {
          winningScore = gameScores.get(player.getName());
          winner = player;
        }
      }
      if (lost) {
        // TODO(killer): update this so that the loggingEventListener handles printing out the
        // winner.
        GameEvent.Builder gameEndEvent = new GameEvent.Builder();
        gameEndEvent.setType(GameEvent.EventType.GAME_END);
        gameEndEvent.setWinningPlayer(winner.getName());
        broadcastEvent(gameEndEvent.build());
        break;
      }
    }
  }

  private void playRound() throws Exception {

    GameState state = new GameState(Deck.getFullDeck());
    gameEventListeners.add(0, state);
    if (gui != null) {
      this.gui.setGameState(state);
    }
    GameEvent event = null;
    state.deal(players);
    // This is so the winner of the last round starts the new round
    int playerIndex = players.indexOf(roundWinner);
    if (playerIndex == -1) {
      playerIndex = 0;
    }

    // Broadcast the starting player and the initial discard.
    GameEvent.Builder roundStartEvent = new GameEvent.Builder();
    roundStartEvent.setType(GameEvent.EventType.ROUND_START);
    roundStartEvent.setStartingPlayer(players.get(playerIndex).getName());
    roundStartEvent.setInitialDiscard(state.getLastDiscard());
    broadcastEvent(roundStartEvent.build());

    for (; true; playerIndex++) {
      Player player = players.get(playerIndex%players.size());
      if (state.getDeckSize() == 0) {
        state.setDeck(refreshDeck(state, event.getDiscard()));
      }
      Discard discard;
      if (event == null) { // First play of the round.
        discard = state.getLastDiscard();
      } else {
        discard = event.getDiscard();
      }
      event = player.takeTurn(state.getCardsInPlayersHand(player.getName()), discard);
      String eventPlayer = event.getPlayer();
      broadcastEvent(event);

      if (event.getType() == GameEvent.EventType.CALLED_ZACH) {
        // Someone called Zach even though they didn't have fewer than 7 points.
        if (state.getValueOfPlayersHand(eventPlayer) > ZACH_THRESHOLD) {
          // Going to give them a penalty and just start a new round.
          // Would like to kick them out, but don't want to modify the
          // players ArrayList while iterating over it.
          gameScores.put(eventPlayer, gameScores.get(eventPlayer) + PENALTY);
          resetPlayers(state);
          gameEventListeners.remove(state);
          return;
        } else {

          // tally the scores and generate the roundEndEvent.
          GameEvent roundEndEvent = processScores(state, eventPlayer);

          broadcastEvent(roundEndEvent);

          resetPlayers(state);
          gameEventListeners.remove(state);
          return;
        }
      }
    }
  }

  private void resetPlayers(GameState state) {
    players.forEach(state::resetHand);
  }

  private Deck refreshDeck(GameState state, Discard discard) {

    // Let all parties know that the deck is being refreshed.
    GameEvent.Builder refreshEvent = new GameEvent.Builder();
    refreshEvent.setType(GameEvent.EventType.DECK_REFRESH);
    broadcastEvent(refreshEvent.build());

    // Get a new full deck
    Deck fullDeck = Deck.getFullDeck();
    Set<Card> cardsToRemove = new TreeSet<>();
    // Remove the cards that players are holding from the full deck
    for (Player tempPlayer : players) {
      cardsToRemove.addAll(state.getCardsInPlayersHand(tempPlayer.getName()));
    }
    // Remove the last played card(s)
    cardsToRemove.addAll(discard.getCards());
    // Actually remove the cards and return refreshedDeck
    return fullDeck.removeCards(cardsToRemove);
  }


  private GameEvent processScores(GameState state, String zachCaller) {
    GameEvent.Builder roundEndEvent = new GameEvent.Builder();
    roundEndEvent.setType(GameEvent.EventType.ROUND_END);

    Integer lowestHand = null;
    List<Player> winners = new ArrayList<>();
    Player winner;

    Map<String, Integer> roundScores = new HashMap<>();
    // Iterate through the players and collect the one(s) that has(have) the lowest score.
    Player zachCallerPlayer = null;
    for (Player player : players) {
      roundScores.put(player.getName(), 0);
      if (player.getName().equals(zachCaller)) {
        zachCallerPlayer = player;
      }
      if (lowestHand == null) {
        lowestHand = state.getValueOfPlayersHand(player);
      }
      if (state.getValueOfPlayersHand(player) <= lowestHand) {
        if (state.getValueOfPlayersHand(player) < lowestHand) {
          winners = new ArrayList<>();
          lowestHand = state.getValueOfPlayersHand(player);
        }
        winners.add(player);
      }
    }

    if (zachCallerPlayer == null) {
      throw new IllegalStateException("processScores was called with a zachCaller that isn't in" +
        "the list of players");
    }
    // Someone got Winged!
    if (!winners.contains(zachCallerPlayer)) {
      gameScores.put(zachCaller, gameScores.get(zachCaller) + state.getValueOfPlayersHand(zachCaller) + PENALTY);
      roundScores.put(zachCaller, state.getValueOfPlayersHand(zachCaller) + PENALTY);
      winner = winners.get((int)(Math.random()*(double)winners.size()));
    } else {
      winner = zachCallerPlayer;
    }

    // Update the players.
    for (Player player : players) {
      if (!player.getName().equals(zachCaller) && !winners.contains(player)) {
        gameScores.put(player.getName(), gameScores.get(player.getName()) + state.getValueOfPlayersHand(player));
        roundScores.put(player.getName(), state.getValueOfPlayersHand(player));
      }
      // If the player's new score is a multiple of 50
      // and either they were not a winner or
      // they organically got 0, we drop the score
      // by 50.
      if(gameScores.get(player.getName()) % 50 == 0 &&
        (!winners.contains(player) || state.getValueOfPlayersHand(player) == 0)) {
        gameScores.put(player.getName(), gameScores.get(player.getName()) + MULTIPLE_OF_FIFTY_REDUCTION);
        roundScores.put(player.getName(), roundScores.get(player.getName()) + MULTIPLE_OF_FIFTY_REDUCTION);
      }
    }

    roundWinner = winner;
    roundEndEvent.setWinningPlayer(winner.getName());
    roundEndEvent.setGameScores(gameScores);
    roundEndEvent.setRoundScores(roundScores);
    return roundEndEvent.build();
  }

  List<String> getPlayerNames() {
    List<String> names = new ArrayList<>();
    names.addAll(players.stream().map(Player::getName).collect(Collectors.toList()));
    return names;
  }

  void broadcastEvent(GameEvent event) {
    for (GameEventListener listener : gameEventListeners) {
      listener.onGameEvent(event);
    }
  }

  public static void main(String[] args) throws Exception {
    List<Player> players = new ArrayList<>();
    players.add(new KillerPlayer("Killer", Arrays.asList(new MinimizationStrategy(0), new HitFiftyStrategy())));
    players.add(new KillerPlayer("Wing", Arrays.asList(new MinimizationStrategy(2), new HitFiftyStrategy())));
    players.add(new KillerPlayer("Zach", Arrays.asList(new MinimizationStrategy(1), new HitFiftyStrategy())));
//    players.add(new WingPlayer("Wing"));
//    players.add(new WingPlayer("Killer"));

    GuiEventListener gui = new GuiEventListener();

    ZachGame game = new ZachGame(players);
    game.playGame();
  }
}