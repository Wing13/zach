import java.util.*;
import java.util.Map.Entry;

public class KillerPlayer extends Player {
  List<Strategy> strategies;
  List<Discard> discards = new ArrayList<>();
  List<Card> pickups = new ArrayList<>();

  // these are the pieces of information we'd like to keep track of for each player.
  // they should probably be wrapped in some container object but I'm lazy.
  List<String> players;
  Map<String, SortedSet<Card>> knownHands;
  Map<String, Integer> playerCardCounts;
  Map<String, List<GameEvent>> playerTurns;
  Map<String, Double> playerHandValues;
  SortedSet<Card> roundStartHand = null;
  boolean roundStartHandInvalid = true;

  protected static final Double multicardBumpFactor = 1.25;

  public KillerPlayer(String name, List<Strategy> strategies) {
    this.name = name;
    this.strategies = strategies;
    this.discards = new ArrayList<>();
    this.knownHands = new HashMap<>();
    this.playerHandValues = new HashMap<>();
    this.playerCardCounts = new HashMap<>();
    this.playerTurns = new HashMap<>();
    for (Strategy strategy : strategies) {
      strategy.setParent(this);
    }
  }

  @Override
  GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {
    if (roundStartHand == null || roundStartHandInvalid) {
      roundStartHand = new TreeSet<>(hand);
      roundStartHandInvalid = false;
    }
    int minimumEstimatedReturnOnStrategy = 50;
    Strategy bestStrategy = null;
    for (Strategy strategy : strategies) {
      int returnOnStrategy = strategy.estimateReturnOnStrategy(hand);
      if (returnOnStrategy < minimumEstimatedReturnOnStrategy || bestStrategy == null) {
        minimumEstimatedReturnOnStrategy = returnOnStrategy;
        bestStrategy = strategy;
      }
    }

    return bestStrategy.takeTurn(hand, lastDiscard);
  }

  @Override
  public void onGameEvent(GameEvent event) {
    for (Strategy strategy : strategies ) {
      strategy.onGameEvent(event);
    }

    switch(event.getType()) {
      case GAME_START:
        players = new ArrayList<>(event.getPlayers());
        break;
      case ROUND_START:
        for (String player : players) {
          knownHands.put(player, new TreeSet<>());
          playerTurns.put(player, new ArrayList<>());
          playerHandValues.put(player, estimateValueOfUnknownCard() * 5);
          playerCardCounts.put(player, 5);
        }
        discards.add(event.getInitialDiscard());
        break;
      case DISCARDED:
        discards.add(event.getDiscard());
        pickups.add(event.getPickedUp());
        knownHands.get(event.getPlayer()).removeAll(event.getDiscard().getCards());
        if(event.getDiscard().getCards().size() > 1) {
          playerCardCounts.put(event.getPlayer(),
              playerCardCounts.get(event.getPlayer()) - (event.getDiscard().getCards().size() - 1));
        }
        if (event.getPickedUp() != null && event.getPickedUp().getValue() != Value.UNKNOWN) {
          knownHands.get(event.getPlayer()).add(event.getPickedUp());
        }
        playerTurns.get(event.getPlayer()).add(event);


        break;
      case CALLED_ZACH:
        discards = new ArrayList<>();
        pickups = new ArrayList<>();
        roundStartHandInvalid = true;
        break;
      case DECK_REFRESH:
        discards = new ArrayList<>();
        break;
      case ROUND_END:
        knownHands = new HashMap<>();
        playerTurns = new HashMap<>();
        playerHandValues = new HashMap<>();
        break;
    }
  }

  protected double estimateHandFromHistoryOfTurns(List<GameEvent> turns) {
    return 0.0;
  }

  protected double estimateHandFromSingleTurnAndKnownCards(GameEvent event, SortedSet<Card> cards) {
    return 0.0;
  }

  protected double probabilityThatPickupIsPartOfAMultiple(Card pickUp) {
    return Math.min(pickUp.getNumericValue()/8, 1);
  }

  protected double estimateValueOfUnknownCard() {
    Set<Card> cardsNotInDeck = new TreeSet<>();
    for (Discard discard : discards) {
      cardsNotInDeck.addAll(discard.getCards());
    }

    for (Card card : pickups) {
      if (card != null && card.getValue() != Value.UNKNOWN) {
        cardsNotInDeck.add(card);
      }
    }

    Set<Card> cardsLeftInDeck = Deck.getFullDeck().cards;
    cardsLeftInDeck.removeAll(cardsNotInDeck);

    int totalValue = 0;
    for(Card card : cardsLeftInDeck) {
      totalValue += card.getNumericValue();
    }

    return (double) totalValue / cardsLeftInDeck.size();
  }

  protected double probabilityOfValue(int value) {
    Set<Card> cardsNotInDeck = new TreeSet<>();
    for (Discard discard : discards) {
      cardsNotInDeck.addAll(discard.getCards());
    }

    for (Card card : pickups) {
      if (card != null && card.getValue() != Value.UNKNOWN) {
        cardsNotInDeck.add(card);
      }
    }

    Set<Card> cardsLeftInDeck = Deck.getFullDeck().cards;
    cardsLeftInDeck.removeAll(cardsNotInDeck);

    int valueFrequency = 0;
    for (Card card : cardsLeftInDeck) {
      if (card.getNumericValue() == value) {
        valueFrequency++;
      }
    }

    return (double) valueFrequency / cardsLeftInDeck.size();
  }

  protected double estimateValueOfUnknownPlayerHandCard() {
    Set<Card> knownCards = new TreeSet<>();
    for(Discard discard : discards) {
      for(Card card : discard.getCards()) {
        knownCards.add(card);
      }
    }

    int totalSumOfDiscard = 0;
    for(Card card : knownCards) {
      totalSumOfDiscard += card.getNumericValue();
    }

    int totalNumberOfDealtCards = knownCards.size();
    for(Entry<String, Integer> cardCount : playerCardCounts.entrySet()) {
      totalNumberOfDealtCards += cardCount.getValue() - knownHands.get(cardCount.getKey()).size();
    }

    double estimatedSumOfDealtCards = (double)totalNumberOfDealtCards * 6.7;

    double estimatedSumOfPlayerCards = estimatedSumOfDealtCards - totalSumOfDiscard;

    int numberOfUnknownCardsInPlayerHands = totalNumberOfDealtCards - knownCards.size();

    return estimatedSumOfPlayerCards/numberOfUnknownCardsInPlayerHands;
  }

  protected double estimateValueOfPlayersHand(String player) {
    int estimate = 0;
    for(Card card : knownHands.get(player)) {
      estimate += card.getNumericValue();
    }
    estimate += (playerCardCounts.get(player) - knownHands.get(player).size()) * estimateValueOfUnknownPlayerHandCard();
    return estimate;
  }

  protected double getStdDevOfEstimate(String player) {
    int numRandomCards = (playerCardCounts.get(player) - knownHands.get(player).size());
    double result = 0.0;
    for (int i = 0; i < numRandomCards; i++) {
      result = Math.sqrt(Math.pow(result, 2) + Math.pow(4.0, 2));
    }
    return result;
  }

  protected double probabilityOfValueInDeck(int value) {
    return probabilityOfValue(value);
  }

  protected double probabilityOfValueInDiscard(int value) {
    return probabilityOfValue(value);
  }

  protected double probabilityOfCardInPlayersHand(Card card, String player) {
    return 0.0;
  }

  protected double probabilityOfCardInDeck(Card card, String player) {
    return 0.0;
  }

  protected double probabilityOfCardInFutureDiscard(Card card) {
    return 0.0;
  }

  SortedSet<Card> getRoundStartHand() {
    return roundStartHand;
  }
}
