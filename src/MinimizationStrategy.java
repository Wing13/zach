import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.distribution.NormalDistribution;

// Basically just try to get as low a score as possible
class MinimizationStrategy extends Strategy {

  private KillerPlayer parent;
  private int turnCount = 0;
  private static final Logger log = Logger.getLogger( HitFiftyStrategy.class.getName() );
  private final int additionalCardBenefit;

  MinimizationStrategy(int additionalCardBenefit) {
    this.additionalCardBenefit = additionalCardBenefit;
  }

  @Override
  int estimateReturnOnStrategy(Set<Card> hand) {
    return 9;
  }

  @Override
  void setParent(KillerPlayer parent) {
    this.parent = parent;
    log.setLevel(Level.FINER);
  }

  double getWingProbabilityThreshold(int handValue) {
    double result = 0;
    if (turnCount < 5) {
      result += .15;
    } else if (turnCount < 10) {
      result += .2;
    } else if (turnCount < 20) {
      result += .25;
    } else if (turnCount < 40) {
      result += .3;
    } else if (turnCount < 80) {
      result += .35;
    } else {
      result += .4;
    }

    if (handValue == 7) {
      result += .1;
    } else if (handValue == 6) {
      result += .15;
    } else if (handValue == 5) {
      result += .2;
    } else if (handValue == 4) {
      result += .25;
    } else if (handValue == 3) {
      result += .3;
    } else if (handValue == 2) {
      result += .35;
    } else if (handValue == 1) {
      result += .40;
    } else {
      result += .45;
    }

    return result;
  }

  @Override
  GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {
    turnCount++;
    GameEvent.Builder gameEvent = new GameEvent.Builder().setPlayer(parent.getName());
    log.fine("Hello Killer's monster, reporting for duty.");
    if (Card.getValueOfCards(hand) <= 7 &&
        probabilityThatSomePlayerHasLessThanMe((double)Card.getValueOfCards(hand)) < getWingProbabilityThreshold(Card.getValueOfCards(hand))) {
      log.fine("The cards in my hand are less than 7. I'm calling Zach.");
      return gameEvent.setType(GameEvent.EventType.CALLED_ZACH).build();
    } else {
      gameEvent.setType(GameEvent.EventType.DISCARDED);
    }

    // See what runs and multiples I currently have
    List<SortedSet<Card>> multiCardDiscards = new ArrayList<>();
    multiCardDiscards.addAll(CommonUtils.detectMultiples(hand));
    multiCardDiscards.addAll(CommonUtils.detectRuns(hand));

    // See if there is a joker in the discard and see if I have what it represents

    Card highestCardRepresentation = null;
    for (Card card : lastDiscard.getJokerValues()) {
      if (hand.contains(card) &&
          (highestCardRepresentation == null ||
              card.getNumericValue() > highestCardRepresentation.getNumericValue())) {
        highestCardRepresentation = card;
      }
    }
    if (highestCardRepresentation != null) {
      for (Card card : lastDiscard.getCards()) {
        if (card.getValue() == Value.JOKER) {
          gameEvent.setPickedUp(card);
          gameEvent.setDiscard(new Discard.Builder().addCard(highestCardRepresentation).build());
          return gameEvent.build();
        }
      }
    }

    // Take it and drop what it represents if so.

    log.fine("These are the cards that I have:");
    for (Card card : hand) {
      log.fine("  " + card.toString());
    }

    log.fine("These are the multiples that I could play:");
    for(SortedSet<Card> discard : multiCardDiscards) {
      for (Card card : discard) {
        log.fine("  " + card.toString());
      }
    }

    // Look at what new runs and multiples the possible pickups would give me.
    List<Card> possiblePickUpCards = CommonUtils.getPossiblePickups(lastDiscard);

    log.fine("These are the cards that are available to be picked up:");
    for (Card card : possiblePickUpCards) {
      log.fine("  " + card.toString());
    }

    Card largestReductionCard = null;
    List<SortedSet<Card>> bestPossibleNewMultiCardDiscards = new ArrayList<>();
    SortedMap<SortedSet<Card>, Card> futureDiscardMap = new TreeMap<>(new Comparator<SortedSet<Card>>(){
      @Override
      public int compare(SortedSet<Card> o1, SortedSet<Card> o2) {
        Integer o1Value = CommonUtils.getWeightedValueOfCards(o1, additionalCardBenefit);
        Integer o2Value = CommonUtils.getWeightedValueOfCards(o2, additionalCardBenefit);
        return o1Value.compareTo(o2Value);
      }
    });

    if (!someoneWillLikelyCallZach()) {
      for (Card card : possiblePickUpCards) {
        SortedSet<Card> possibleNewHand = new TreeSet<>(hand);
        possibleNewHand.add(card);

        List<SortedSet<Card>> possibleNewMultiCardDiscards = new ArrayList<>();
        possibleNewMultiCardDiscards.addAll(CommonUtils.detectMultiples(possibleNewHand));
        possibleNewMultiCardDiscards.addAll(CommonUtils.detectRuns(possibleNewHand));

        List<SortedSet<Card>> trimmedNewMultiCardDiscards = new ArrayList<>();
        for (SortedSet<Card> cards : possibleNewMultiCardDiscards) {
          if (cards.contains(card)) {
            trimmedNewMultiCardDiscards.add(cards);
          }
        }

        for (SortedSet<Card> discard : trimmedNewMultiCardDiscards) {
          futureDiscardMap.put(discard, card);
        }

        log.fine("These are the new multiples I could have if I drew the: " + card.toString());
        for (SortedSet<Card> discard : trimmedNewMultiCardDiscards) {
          for (Card newDiscardCard : discard) {
            log.fine("  " + newDiscardCard.toString());
          }
        }
      }

      for (Entry<SortedSet<Card>, Card> futureDiscard : futureDiscardMap.entrySet()) {
        SortedSet<Card> existingCards = new TreeSet<>(futureDiscard.getKey());
        existingCards.remove(futureDiscard.getValue());

        boolean hasLargerCurrentDiscards = false;
        for (Card existingCard : existingCards) {
          for (SortedSet<Card> currentDiscard : multiCardDiscards) {

            // We have bigger plans for this card.
            if (currentDiscard.contains(existingCard) &&
                CommonUtils.getWeightedValueOfCards(currentDiscard, additionalCardBenefit) >
                    CommonUtils
                        .getWeightedValueOfCards(futureDiscard.getKey(), additionalCardBenefit)) {
              hasLargerCurrentDiscards = true;
              break;
            }
          }
          if (hasLargerCurrentDiscards) {
            break;
          }
        }
        if (!hasLargerCurrentDiscards) {
          largestReductionCard = futureDiscard.getValue();
          bestPossibleNewMultiCardDiscards.add(futureDiscard.getKey());
        }
      }

      if (largestReductionCard != null) {
        log.fine("I think the best card for me would be the" + largestReductionCard.toString());
      }
    }

    SortedSet<Card> trimmedHand = new TreeSet<>();
    while (trimmedHand.isEmpty()) {
      trimmedHand.addAll(hand);
      if (bestPossibleNewMultiCardDiscards != null) {
        bestPossibleNewMultiCardDiscards.forEach(trimmedHand::removeAll);
        log.fine("These are the discards this thing enables:");
        for (SortedSet<Card> discard : bestPossibleNewMultiCardDiscards) {
          for (Card card : discard) {
            log.fine("  " + card.toString());
          }
        }
      }

      log.fine("These are the cards that I have after trimming:");
      for (Card card : trimmedHand) {
        log.fine("  " + card.toString());
      }
      log.fine("This was the original hand:");
      for (Card card : hand) {
        log.fine("  " + card.toString());
      }

      if(trimmedHand.isEmpty()) {
        int smallestValue = 60;
        SortedSet<Card> worstMultiple = null;
        assert bestPossibleNewMultiCardDiscards != null;
        for (SortedSet<Card> multiple : bestPossibleNewMultiCardDiscards) {
          int value = 0;
          for(Card card : multiple) {
            value += card.getNumericValue();
          }
          if(worstMultiple == null || value < smallestValue) {
            smallestValue = value;
            worstMultiple = multiple;
          }
        }

        log.fine("This is the thing that should be removed from the thing:");
        for (Card card : worstMultiple) {
          log.fine("  " + card.toString());
        }
        boolean successfullyRemoved = bestPossibleNewMultiCardDiscards.remove(worstMultiple);
        assert successfullyRemoved;
      }
    }

    List<SortedSet<Card>> trimmedMultiCardDiscards = new ArrayList<>();
    trimmedMultiCardDiscards.addAll(CommonUtils.detectMultiples(trimmedHand));
    trimmedMultiCardDiscards.addAll(CommonUtils.detectRuns(trimmedHand));
    trimmedMultiCardDiscards.addAll(CommonUtils.getSingles(trimmedHand));

    Discard.Builder discard = new Discard.Builder();
    if(!trimmedMultiCardDiscards.isEmpty()) {
      int maxValue = 0;
      SortedSet<Card> bestMultiple = null;
      for(SortedSet<Card> trimmedMultiCardDiscard : trimmedMultiCardDiscards) {
        int sum = CommonUtils.getWeightedValueOfCards(trimmedMultiCardDiscard,
            additionalCardBenefit);
        if (bestMultiple == null || sum > maxValue) {
          bestMultiple = trimmedMultiCardDiscard;
          maxValue = sum;
        }
      }
      // bestMultiple should be the discard!
      List<Card> jokerValues;
      if (bestMultiple.size() >= 3) {
        CommonUtils.SequenceDetectionResult result =
            CommonUtils.detectRun(new ArrayList<>(bestMultiple));
        jokerValues = result.jokerValues;
      } else {
        jokerValues = new ArrayList<>(Deck.getFullDeck().cards);
        jokerValues.removeAll(Card.JOKERS);
      }

      int jokerIndex = 0;
      for (Card card : bestMultiple) {
        if (card.getValue() == Value.JOKER) {
          discard.addJoker(card, jokerValues.get(jokerIndex));
          jokerIndex++;
        } else {
          discard.addCard(card);
        }
      }
    } else {
      if(trimmedHand.last().getValue() == Value.JOKER) { // should never happen!
        discard.addJoker(trimmedHand.last(), new Card(Value.ACE, Suit.SPADES));
      } else {
        discard.addCard(trimmedHand.last());
      }
    }
    gameEvent.setDiscard(discard.build());

    if (bestPossibleNewMultiCardDiscards != null && !bestPossibleNewMultiCardDiscards.isEmpty()) {
      gameEvent.setPickedUp(largestReductionCard);
    } else {
      Card leastCard = null;
      for (Card card : possiblePickUpCards) {
        if (leastCard == null || card.getNumericValue() < leastCard.getNumericValue()) {
          leastCard = card;
        }
      }
      if (leastCard != null) {
        SortedSet<Card> newHand = new TreeSet<>(hand);
        newHand.removeAll(discard.build().getCards());
        newHand.add(leastCard);
        if (Card.getValueOfCards(newHand) <= 6) {
          gameEvent.setPickedUp(leastCard);
        } else if (leastCard.getNumericValue() < 5 && !leastCard.equals(newHand.last())
            || someoneWillLikelyCallZach() && leastCard.getNumericValue() < parent.estimateValueOfUnknownCard()) {
          gameEvent.setPickedUp(leastCard);
        }
      }
    }

    // Subtract the new runs and multiples from my hand.

    // Are there any cards remaining?

    // If so, drop the largest value multiple or single card

    // If not, go through the new runs and multiples and find the one that has the lowest future
    // value and break that one.

    // If the pickup added new runs / multiples or is under 5 points pick it up

    // Otherwise draw a rando

    return gameEvent.build();
  }

  @Override
  public void onGameEvent(GameEvent event) {
    if(event.getType() == GameEvent.EventType.ROUND_END) {
      turnCount = 0;
    }
  }

  boolean someoneWillLikelyCallZach() {
    List<String> otherPlayers = new ArrayList<>();
    for(String player : parent.players) {
      if(!player.equals(parent.getName())) {
        otherPlayers.add(player);
      }
    }
    return probabilityThatPlayersHaveLessThan(otherPlayers, 7.0) > .5;
  }

  double probabilityThatSomePlayerHasLessThanMe(double handValue) {
    List<String> otherPlayers = new ArrayList<>();
    for(String player : parent.players) {
      if(!player.equals(parent.getName())) {
        otherPlayers.add(player);
      }
    }
    return probabilityThatPlayersHaveLessThan(otherPlayers, handValue);
  }

  // find the probability that no one has less than and take the compliment which should give you
  // the probability that at least one person has less than.
  double probabilityThatPlayersHaveLessThan(List<String> players, double handValue) {
    double probabilityThatItWontHappen = 1.0;
    for (String player: players) {
      probabilityThatItWontHappen *= 1.0 - probabilityThatPlayerHasLessThan(player, handValue);
    }
    return 1.0 - probabilityThatItWontHappen;
  }

  double probabilityThatPlayerHasLessThan(String player, double handValue) {
    double handEstimate = parent.estimateValueOfPlayersHand(player);
    double stdDev = parent.getStdDevOfEstimate(player);

    if (stdDev > 0) {
      NormalDistribution handValueProbability = new NormalDistribution(handEstimate, stdDev);
      return handValueProbability.cumulativeProbability(handValue);
    } else {
      if (handEstimate <= handValue) {
        return 1.0;
      } else {
        return 0.0;
      }
    }
  }
}