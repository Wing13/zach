import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.CountDownLatch;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;


class GuiCard extends JPanel implements ActionListener, Comparable<GuiCard> {
  public final Card card;
  public JLabel label;
  public Point goalLocation;
  Timer tm = new Timer(10, this);
  int PERMANENT_TEMP_VELOCITY = 4;
  public static final int CARD_WIDTH = 60;
  public static final int CARD_HEIGHT = 90;
  private boolean faceUp = false;

  // this is the space between two adjacent cards in front of a player.
  public static final int CARD_CARD_GAP = CARD_WIDTH/3;

  GuiCard(Card card, Point loc) {
    this(card, loc.x, loc.y);
  }

  GuiCard(Card card, int locX, int locY) {
    this.card = card;
    setSize(CARD_WIDTH, CARD_HEIGHT);
    setLocation(locX, locY);
    goalLocation = new Point(locX, locY);
    setBackground(Color.WHITE);
    label = getFaceDownLabel();
    this.add(label);
  }

  void setGoalLocation(Point loc) {
    goalLocation = new Point(loc.x, loc.y);
    tm.start();
  }

  public static List<GuiCard> createGuiCardsFromCards(Iterable<Card> cards, Point loc) {
    List<GuiCard> guiCards = new ArrayList<>();
    for (Card card : cards) {
      guiCards.add(new GuiCard(card, loc));
    }
    return guiCards;
  }

  public static List<GuiCard> createSpreadGuiCardsFromCards(Iterable<Card> cards, Point loc) {
    List<GuiCard> guiCards = new ArrayList<>();
    int offsetX = 0;
    for (Card card : cards) {
      guiCards.add(new GuiCard(card, loc.x + offsetX, loc.y));
      offsetX += GuiCard.CARD_WIDTH + GuiCard.CARD_CARD_GAP;
    }
    return guiCards;
  }

  public static String getBriefCardString(Card card) {
    if (card == null) {
      throw new NullPointerException("Null card values are not allowed.");
    }
    if (card.getValue() == Value.UNKNOWN) {
      throw new IllegalArgumentException("Unknown cards are not allowed.");
    }
    StringBuilder sb = new StringBuilder();
    if (card.getValue() == Value.JOKER) {
      sb.append("JKR");
    } else {
      if (card.getValue().ordinal() + 1 >= 2 && card.getValue().ordinal() + 1 <= 10) {
        sb.append(card.getNumericValue());
      } else {
        sb.append(card.getValue().name().charAt(0));
      }

      switch (card.getSuit()) {
        case SPADES:
          sb.append("♠");
          break;
        case HEARTS:
          sb.append("♥");
          break;
        case DIAMONDS:
          sb.append("♦");
          break;
        case CLUBS:
          sb.append("♣");
          break;
        default:
          throw new AssertionError("This shouldn't be possible.");
      }
    }
    return sb.toString();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Point loc = getLocation();
    Point differenceVector = new Point(goalLocation.x - loc.x, goalLocation.y - loc.y);
    double distance = loc.distance(goalLocation);
    Point motionVector = new Point(
        (int) (PERMANENT_TEMP_VELOCITY * differenceVector.x / distance),
        (int) (PERMANENT_TEMP_VELOCITY * differenceVector.y / distance));

    if (distance < PERMANENT_TEMP_VELOCITY) {
      setLocation(goalLocation.x, goalLocation.y);
    } else {
      setLocation(loc.x + motionVector.x, loc.y + motionVector.y);
    }

    if (loc.equals(goalLocation)) {
      tm.stop();
    }
    repaint();
  }

  @Override
  public int compareTo(GuiCard o) {
    if (o.faceUp && !this.faceUp) {
      return 1;
    }
    if (this.faceUp && !o.faceUp) {
      return -1;
    }
    return card.compareTo(o.card);
  }

  public void flip() {
    this.remove(this.label);
    if (faceUp) {
      this.label = getFaceDownLabel();
    } else {
      this.label = getFaceUpLabel();
    }
    this.add(this.label);
    faceUp = !faceUp;
  }

  public JLabel getFaceUpLabel() {
    JLabel newLabel = new JLabel();
    if (card.getSuit() == Suit.UNKNOWN) {
      return getFaceDownLabel();
    }
    if (card.getSuit() == Suit.DIAMONDS || card.getSuit() == Suit.HEARTS) {
      // Forground is the font color
      newLabel.setForeground(Color.RED);
    }
    newLabel.setFont(new Font(label.getFont().getName(), Font.PLAIN, 20));
    newLabel.setText(getBriefCardString(card));
    return newLabel;
  }

  public JLabel getFaceDownLabel() {
    JLabel newLabel = new JLabel();
    // Set flow layout to get rid of a gap between the top of the card and the image of the back.
    FlowLayout layout = new FlowLayout();
    layout.setVgap(0);
    setLayout(layout);
    try {
      BufferedImage cardBack = ImageIO.read(new File("img/card_back_blue.jpg"));
      Image dimg = cardBack.getScaledInstance(GuiCard.CARD_WIDTH, GuiCard.CARD_HEIGHT,
          Image.SCALE_SMOOTH);
      newLabel.setIcon(new ImageIcon(dimg));
    } catch (IOException e) {
      // Whatever, lol.
    }
    return newLabel;
  }

  public boolean getFaceUp() {
    return faceUp;
  }
}

class GuiPlayer extends JPanel {

  private Player player;
  private Point direction;
  private Point loc;
  private Point centerCardLoc;
  private List<GuiCard> cards = new ArrayList<>();

  JLabel name;

  private final int CARD_PLAYER_GAP = GuiCard.CARD_HEIGHT; // this is the space between the player and the cards in
  // front of them.

  GuiPlayer(Player player, Point direction, Point loc) {
    this.player = player;

    // TODO: This should probably be normalized as we get it.
    this.direction = direction;
    this.loc = loc;

    name = new JLabel();
    name.setForeground(Color.BLACK);
    name.setText(player.getName());
    this.add(name);

    setLocation(loc.x - 50, loc.y - 15);
    setSize(100,30);

    Point centerCardCenter = new Point(loc.x + direction.x * CARD_PLAYER_GAP, loc.y + direction.y * CARD_PLAYER_GAP);
    this.centerCardLoc = new Point(centerCardCenter.x - GuiCard.CARD_WIDTH / 2,
        centerCardCenter.y - GuiCard.CARD_HEIGHT / 2);
  }

  Player getPlayer() {
    return player;
  }

  Point getCardLoc(int index) {
    int indexDiff = index - 2;
    int horizontalOffset = indexDiff * (GuiCard.CARD_CARD_GAP + GuiCard.CARD_WIDTH);
    return new Point(centerCardLoc.x + horizontalOffset, centerCardLoc.y);
  }

  void addGuiCard(GuiCard card) {
    cards.add(card);
  }

  void removeGuiCard(GuiCard card) {
    cards.remove(card);
  }

  void updatePositions() {
    int index = 0;
    Collections.sort(cards);
    for (GuiCard card : cards) {
      card.setGoalLocation(getCardLoc(index));
      index++;
    }
  }

  Collection<GuiCard> getCards() {
    return cards;
  }

  void clearHand() {
    cards = new ArrayList<>();
  }
}
public class GuiEventListener extends JFrame implements GameEventListener, KeyListener {

  JPanel announcement;
  JLabel announcementText;

  JPanel scoreBox;
  JLabel scoreBoxText;

  List<GuiCard> allGuiCards = new ArrayList<>();
  List<GuiCard> previousDiscardGuiCards = new ArrayList<>();
  Map<Card, GuiCard> guiCardLookUpTable = new HashMap<>();
  List<GuiPlayer> players;
  GameState gameState;
  String zachCaller;
  List<Map<String, Integer>> previousRoundScores = new ArrayList<>();
  CountDownLatch roundEndLatch = new CountDownLatch(1);

  // Size + Location Constants

  public static final int WINDOW_WIDTH = 1100;
  public static final int WINDOW_HEIGHT = 900;

  public static final Point deckLocation = new Point(500, WINDOW_HEIGHT/2 - GuiCard.CARD_HEIGHT/2);
  public static final Point discardLocation = new Point(600, WINDOW_HEIGHT/2 - GuiCard.CARD_HEIGHT/2);

  public static final int ANNOUNCEMENT_HEIGHT = 300;
  public static final int ANNOUNCEMENT_WIDTH = 400;

  public static final int SCORE_BOX_WIDTH = 300;
  public static final int SCORE_BOX_HEIGHT = 100;
  public static final int SCORE_BOX_X = WINDOW_WIDTH/10;
  public static final int SCORE_BOX_Y = (9*WINDOW_HEIGHT)/10;

  // Animation Constants

  public static final int BETWEEN_EVENT_SLEEP_MILLIS = 3000;
  public static final int AFTER_CARD_FLIP_DELAY_MILLIS = 2000;
  public static final int BEFORE_DISCARD_FLIP_DELAY_MILLIS = 500;

  GuiEventListener() {
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(null);
    setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

    // TODO: is there a way to center this on the screen?
    setLocation(200, 100);
    getContentPane().setBackground(new Color(0,80,0));

    announcement = new JPanel();
    announcement.setSize(ANNOUNCEMENT_WIDTH, ANNOUNCEMENT_WIDTH);
    announcement.setLocation((WINDOW_WIDTH/2)-(ANNOUNCEMENT_WIDTH/2),
        (WINDOW_HEIGHT/2)-(ANNOUNCEMENT_HEIGHT/2));

    announcementText = new JLabel();
    announcement.add(announcementText);

    scoreBox = new JPanel();
    scoreBox.setSize(SCORE_BOX_WIDTH, SCORE_BOX_HEIGHT);
    scoreBox.setLocation(SCORE_BOX_X, SCORE_BOX_Y);

    scoreBoxText = new JLabel();
    scoreBox.add(scoreBoxText);

    this.add(scoreBox);

    this.addKeyListener(this);

    this.add(announcement);
    announcement.setVisible(false);
  }

  void setPlayers(List<Player> players) {
    this.players = new ArrayList<>();
    int direction = -1;
    int index = 0;
    Map<String, Integer> scores = new HashMap<>();
    for (Player player : players) {
      GuiPlayer guiPlayer = new GuiPlayer(player, new Point(0,direction), getPlayerLocations(players.size()).get(index));
      scores.put(player.getName(), 0);
      this.players.add(guiPlayer);
      this.add(guiPlayer);
      index++;
      direction *= -1;
    }

    scoreBoxText.setText(getHTMLTableOfScores(scores));
  }

  List<Point> getPlayerLocations(int numPlayers) {
    List<Point> playerLocations = new ArrayList<>();
    int topPlayerLine = WINDOW_HEIGHT/5;
    int bottomPlayerLine = (4 * WINDOW_HEIGHT)/5;

    int numTopPlayers = (int) Math.floor(numPlayers/2);
    int numBottomPlayers = (int) Math.ceil((float)numPlayers/2);

    int topPlayerSpace = WINDOW_WIDTH/numTopPlayers;
    int bottomPlayerSpace = WINDOW_WIDTH/numBottomPlayers;

    int topPlayerOffset = topPlayerSpace/2;
    int bottomPlayerOffset = bottomPlayerSpace/2;

    for(int i = 0; i < numPlayers; i++) {
      if (i%2 == 0) {
        playerLocations.add(new Point(bottomPlayerOffset, bottomPlayerLine));
        bottomPlayerOffset += bottomPlayerSpace;
      } else {
        playerLocations.add(new Point(topPlayerOffset, topPlayerLine));
        topPlayerOffset += topPlayerSpace;
      }
    }
    return playerLocations;
  }

  String getHTMLTableOfScores(Map<String, Integer> scores) {
    StringBuilder sb = new StringBuilder();
    sb.append("<html>");
    sb.append("<table>");
    sb.append("<tr>");
    for (String name : scores.keySet()) {
      sb.append("<th>" + name + "</th>");
    }
    sb.append("</tr>");
    sb.append("<tr>");
    for (Integer score : scores.values()) {
      sb.append("<td>");
      sb.append(score);
      sb.append("</td>");
    }
    sb.append("</tr>");
    return sb.toString();
  }

  String getHTMLTableOfRoundScores(List<Map<String, Integer>> roundScores, Map<String, Integer> gameScores) {
    StringBuilder sb = new StringBuilder();
    sb.append("<table>");
    sb.append("<tr>");
      sb.append("<th></th>");
    for (String name : roundScores.get(0).keySet()) {
      sb.append("<th>" + name + "</th>");
    }
    sb.append("</tr>");

    for (int i = Math.max(0, roundScores.size() - 10); i < roundScores.size(); i++) {
      sb.append("<tr>");
      sb.append("<th>");
      sb.append(i+1);
      sb.append("</th>");
      for (Integer score : roundScores.get(i).values()) {
        sb.append("<td>");
        sb.append(score);
        sb.append("</td>");
      }
      sb.append("</tr>");
    }
    sb.append("<tr>");
    sb.append("<th>");
    sb.append("Total");
    sb.append("</th>");
    for (Integer score : gameScores.values()) {
      sb.append("<td style=\"border-top: thin solid;\">");
      sb.append(score);
      sb.append("</td>");
    }
    sb.append("</tr>");
    sb.append("</table>");
    return sb.toString();
  }

  void setGameState(GameState state) {
    this.gameState = state;
  }

  @Override
  public void onGameEvent(GameEvent event) {

    switch (event.getType()) {
      case GAME_START:
        // Create a pile card.
        GuiCard pileCard = new GuiCard(new Card(Value.UNKNOWN, Suit.UNKNOWN), deckLocation);
        allGuiCards.add(pileCard);
        this.add(pileCard);
        uiPause(BETWEEN_EVENT_SLEEP_MILLIS);
        break;

      case ROUND_START:

        zachCaller = null;
        announcement.setVisible(false);

        // clears the old cards away.
        repaint();

        // Create a pile card.
        GuiCard roundPileCard = new GuiCard(new Card(Value.UNKNOWN, Suit.UNKNOWN), deckLocation);
        allGuiCards.add(roundPileCard);
        this.add(roundPileCard);

        // Put the discard there
        List<GuiCard> discards = GuiCard.createGuiCardsFromCards(event.getInitialDiscard().getCards(),
            deckLocation);
        allGuiCards.addAll(discards);
        previousDiscardGuiCards = discards;
        for(GuiCard discard : discards) {
          this.add(discard, 0);
          discard.setGoalLocation(discardLocation);
          guiCardLookUpTable.put(discard.card, discard);
        }

        uiPause(BEFORE_DISCARD_FLIP_DELAY_MILLIS);

        // Flips it and immediately revalidates so that the front of the
        // discard is shown.
        for (GuiCard discard : discards) {
          discard.flip();
        }
        revalidate();

        for(int sentinel = 0; sentinel < players.size(); sentinel++) {
          List<GuiCard> cards = GuiCard.createGuiCardsFromCards(gameState.getCardsInPlayersHand(players.get(sentinel).getPlayer()),
              deckLocation);
          allGuiCards.addAll(cards);
          for (GuiCard card : cards) {
            this.add(card, 0);
            players.get(sentinel).addGuiCard(card);
            guiCardLookUpTable.put(card.card, card);
          }
          players.get(sentinel).updatePositions();
        }
        uiPause(BETWEEN_EVENT_SLEEP_MILLIS);
        break;
      case DISCARDED:
        GuiPlayer guiPlayer = null;
        // Flip over unknown cards
        for (GuiPlayer player : players) {
          if (player.getPlayer().getName().equals(event.getPlayer())) {
            guiPlayer = player;
            for (GuiCard card : player.getCards()) {
              if (!card.getFaceUp()) {
                card.flip();
              }
            }
            player.updatePositions();
          }
        }

        uiPause(AFTER_CARD_FLIP_DELAY_MILLIS);

        // Move the previous discard pile to the end so that
        // other cards cover it.
        for (int index = previousDiscardGuiCards.size()-1; index >= 0; index--) {
          this.remove(previousDiscardGuiCards.get(index));
          this.add(previousDiscardGuiCards.get(index));
        }

        // Find and move discarded cards to discard pile
        int offset = 0;
        List<GuiCard> newDiscard = new ArrayList<>();
        for (Card card : event.getDiscard().getCards()) {
          Point discardPosition = new Point(discardLocation.x + offset, discardLocation.y);
          GuiCard guiCard = guiCardLookUpTable.get(card);
          if (guiCard == null) {
            throw new RuntimeException(card.toString());
          }
          newDiscard.add(guiCard);

          guiCard.setGoalLocation(discardPosition);
          offset += GuiCard.CARD_WIDTH + GuiCard.CARD_CARD_GAP;
          guiPlayer.removeGuiCard(guiCard);
        }

        for (GuiCard card : previousDiscardGuiCards) {
          card.setGoalLocation(discardLocation);
        }

        for (GuiCard card : newDiscard) {
          previousDiscardGuiCards.add(card);
        }

        // Pull picked up card into hand
        GuiCard guiCard = null;
        if (event.getPickedUp() == null || event.getPickedUp().getSuit() == Suit.UNKNOWN) {
          for (Card card : gameState.getCardsInPlayersHand(event.getPlayer())) {
            if (guiCardLookUpTable.get(card) == null) {
              guiCard = new GuiCard(card, deckLocation.x, deckLocation.y);
              allGuiCards.add(guiCard);
              this.add(guiCard, 0);
              guiCardLookUpTable.put(card, guiCard);
            }
          }
        } else {
          guiCard = guiCardLookUpTable.get(event.getPickedUp());
          previousDiscardGuiCards.remove(guiCard);
          if (guiCard == null) {
            throw new AssertionError("The discard did not have a corresponding GuiCard");
          }
        }

        guiPlayer.addGuiCard(guiCard);
        guiPlayer.updatePositions();
        uiPause(BETWEEN_EVENT_SLEEP_MILLIS);
        break;
      case CALLED_ZACH:
        // Flip over all unknown cards.
        for (GuiPlayer player : players) {
          for (GuiCard card : player.getCards()) {
            if (!card.getFaceUp()) {
              card.flip();
            }
          }
          player.updatePositions();
        }

        zachCaller = event.getPlayer();
        uiPause(BETWEEN_EVENT_SLEEP_MILLIS);
        break;
      case ROUND_END:
        for (GuiCard card : allGuiCards) {
          this.remove(card);
        }
        for (GuiPlayer player : players) {
          player.clearHand();
        }
        allGuiCards = new ArrayList<>();
        previousDiscardGuiCards = new ArrayList<>();
        guiCardLookUpTable = new HashMap<>();

        previousRoundScores.add(event.getRoundScores());

        StringBuilder sb = new StringBuilder();
        sb.append("<html>");

        if (event.getWinningPlayer().equals(zachCaller)) {
          sb.append(event.getWinningPlayer() + " called zach and won the round.");
        } else {
          sb.append(zachCaller + " called zach but " + event.getWinningPlayer() + " wing'd them.");
        }

        sb.append("<br />");
        sb.append("<br />");
        sb.append(getHTMLTableOfRoundScores(previousRoundScores, event.getGameScores()));
        sb.append("</html>");

        announcementText.setText(sb.toString());
        announcement.setSize(ANNOUNCEMENT_WIDTH, (int)announcementText.getPreferredSize().getHeight() + 10);
        announcement.setLocation((WINDOW_WIDTH/2)-(announcement.getWidth()/2),
            (WINDOW_HEIGHT/2)-(announcement.getHeight()/2));

        scoreBoxText.setText(getHTMLTableOfScores(event.getGameScores()));

        this.remove(announcement);
        this.add(announcement, 0);
        announcement.setVisible(true);
        revalidate();
        roundEndLatch = new CountDownLatch(1);
        try {
          roundEndLatch.await();
        } catch (InterruptedException e) {
          throw new RuntimeException("Shit's Fucked.", e);
        }
    }
  }

  private void uiPause(int timeInMillis) {
    revalidate();
    try {
      Thread.sleep(timeInMillis);
    } catch (InterruptedException e) {
      // LAWLs
    }
  }

  @Override
  public void keyTyped(KeyEvent e) {
  }

  @Override
  public void keyPressed(KeyEvent e) {
    roundEndLatch.countDown();
  }

  @Override
  public void keyReleased(KeyEvent e) {
  }
}