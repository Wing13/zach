import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;
import java.util.Set;

class Deck {
  Set<Card> cards;

  private Deck(Set<Card> cards) {
  this.cards = cards;
  }

  static Deck getFullDeck() {
    Set<Card> cards = new TreeSet<>();
    for (Suit s : Suit.values()) {
      if (s == Suit.JOKER_1 || s == Suit.JOKER_2 || s == Suit.UNKNOWN) {
        continue;
      }

      for (Value v : Value.values()) {
        if(v == Value.JOKER || v == Value.UNKNOWN) {
          continue;
        }
        cards.add(new Card(v, s));
      }
    }
    cards.add(new Card(Value.JOKER, Suit.JOKER_1));
    cards.add(new Card(Value.JOKER, Suit.JOKER_2));

    return new Deck(cards);
  }

  Card drawRandomCard() {
    int rand = new Random().nextInt(cards.size());
    Iterator<Card> cardsIterator = cards.iterator();
    int counter = 0;
    while (counter < rand) {
      counter++;
      cardsIterator.next();
    }
    Card removedCard = cardsIterator.next();
    cards.remove(removedCard);
    return removedCard;
  }

  public Card randomlyDealCard() {
    return this.drawRandomCard();
  }

  public Deck removeCards(Set<Card> cardsToRemove) {
    cards.removeAll(cardsToRemove);
    return this;
  }
}
