interface GameEventListener {
  void onGameEvent(GameEvent event);
}