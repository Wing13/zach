
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

final public class Discard {
  private Set<Card> cards;
  private Set<Card> jokerValues;
  private boolean cardHasBeenRemoved = false;

  private Discard(Set<Card> cards, Set<Card> jokerValues) {
    this.cards = cards;
    this.jokerValues = jokerValues;
  }

  static Discard getInitialJokerDiscard(Card card) {
    if (card.getValue() != Value.JOKER) {
      throw new IllegalArgumentException("This method is only valid for Jokers.");
    }
    Set<Card> cards = new TreeSet<>();
    cards.add(card);
    return new Discard(cards, Deck.getFullDeck().cards);
  }

  public static class Builder {

    private Set<Card> cards;
    private Set<Card> jokerValues;

    public Builder() {
      cards = new TreeSet<>();
      jokerValues = new TreeSet<>();
    }

    public Builder addCard(Card card) {
      if (card == null) {
        return this;
      }
      if (card.getValue() == Value.JOKER){
        throw new IllegalArgumentException("Jokers must have a value specified. Use the addJoker" +
            "method to add a joker to your discard.");
      }
      if (card.getValue() == Value.UNKNOWN) {
        throw new IllegalArgumentException("Cannot discard Unknown cards.");
      }
      cards.add(card);
      return this;
    }

    public Builder addJoker(Card joker, Card jokerRepresentation) {
      if (joker == null && jokerRepresentation == null) {
        return this;
      }
      if (joker == null || joker.getValue() == Value.UNKNOWN) {
        throw new IllegalArgumentException("In order to represent another card, you need to " +
            "discard a Joker card.");
      }
      if (jokerRepresentation == null || jokerRepresentation.getValue() == Value.UNKNOWN ||
          jokerRepresentation.getValue() == Value.JOKER) {
        throw new IllegalArgumentException("Jokers must have a defined value and cannot represent a" +
            " joker.");
      }
      cards.add(joker);
      jokerValues.add(jokerRepresentation);
      return this;
    }

    public Discard build() {
      String errors = validate();
      if (errors.isEmpty()) {
        return new Discard(cards, jokerValues);
      } else {
        throw new IllegalStateException(errors);
      }
    }

    // TODO(killer): actually do validation
    private String validate() {
      if (cards.isEmpty()) {
        return "Discards may not be empty.";
      }

      Set<Card> jokers = new TreeSet<>();
      Set<Card> effectiveCards = new TreeSet<>(jokerValues);
      for (Card card : cards) {
        if (card.getValue() == Value.JOKER) {
          jokers.add(card);
        } else {
          effectiveCards.add(card);
        }
      }
      if (jokers.size() > jokerValues.size()) {
        return "There are unbound jokers.";
      } else if (jokers.size() < jokerValues.size()) {
        return "There are more joker values than joker cards.";
      }

      if (cards.size() == 1) {
        return "";
      } else if (!Card.isRun(effectiveCards) && !Card.isMultiple(effectiveCards)) {
        return "The set of card is neither a run nor a multiple.";
      }
      return "";
    }
  }

  public Set<Card> getCards() {
    return Collections.unmodifiableSet(cards);
  }

  public Set<Card> getJokerValues() {
    return Collections.unmodifiableSet(jokerValues);
  }

  Card removeCard(Card card) {
    if (card == null) {
      return null;
    }

    if (cardHasBeenRemoved) {
      throw new IllegalStateException("A card has already been removed from this discard.");
    }

    if (cards.remove(card)) {
      cardHasBeenRemoved = true;
      return card;
    }
    throw new IllegalArgumentException("You attempted to remove a card that doesn't exist in the " +
        "discard pile.");
  }
}
