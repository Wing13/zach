import java.util.*;

class CommonUtils {
  private CommonUtils() {}

  public enum CardSequenceType {RUN, MULTIPLE}
  public static class SequenceDetectionResult {
    public final CardSequenceType type;
    public final boolean status;
    public final List<Card> jokerValues;
    public final String errorDescription;

    public SequenceDetectionResult(CardSequenceType type, boolean status, List<Card> jokerValues) {
      this.type = type;
      this.status = status;
      this.jokerValues = jokerValues;
      this.errorDescription = "";
    }

    public SequenceDetectionResult(CardSequenceType type, boolean status, List<Card> jokerValues,
                                   String errorDescription) {
      this.type = type;
      this.status = status;
      this.jokerValues = jokerValues;
      this.errorDescription = errorDescription;
    }
  }

  static List<SortedSet<Card>> getSingles(Set<Card> cards) {
    List<SortedSet<Card>> result = new ArrayList<>();
    for (Card card : cards) {
      SortedSet<Card> singleCardSet = new TreeSet<>();
      singleCardSet.add(card);
      result.add(singleCardSet);
    }
    return result;
  }

  static List<SortedSet<Card>> detectMultiples(Set<Card> cards) {
    HashMap<Value, SortedSet<Card>> multiples = new HashMap<>();

    for (Card card : cards) {
      if (!multiples.containsKey(card.getValue())) {
        multiples.put(card.getValue(), new TreeSet<>());
      }
      multiples.get(card.getValue()).add(card);
    }

    List<SortedSet<Card>> actualMultiples = new ArrayList<>();
    for (SortedSet<Card> multiple : multiples.values()) {
      if (multiple.size() > 1) {
        actualMultiples.add(multiple);
      }
    }
    return actualMultiples;
  }

  static int getWeightedValueOfCards(Iterable<Card> cards, int baseCardValue) {
    int result = 0;
    for (Card card : cards) {
      result += card.getNumericValue();
      result += baseCardValue;
    }
    return result;
  }

  static List<SortedSet<Card>> detectRuns(Set<Card> cards) {
    HashMap<Suit, SortedSet<Card>> runs = new HashMap<>();

    List<Card> jokers = new ArrayList<>();
    for (Card card : cards) {
      if (card.getValue() == Value.JOKER) {
        jokers.add(card);
        continue;
      }
      if (!runs.containsKey(card.getSuit())) {
        runs.put(card.getSuit(), new TreeSet<>());
      }
      runs.get(card.getSuit()).add(card);
    }

    List<SortedSet<Card>> actualRuns = new ArrayList<>();
    for (SortedSet<Card> run : runs.values()) {
      Card lastCard = null;
      SortedSet<Card> potentialRun = new TreeSet<>();
      List<Card> tempJokers = new ArrayList<>(jokers);
      for (Card card : run) {
        if (lastCard != null) {
          int gapSize = (card.getValue().ordinal() - lastCard.getValue().ordinal()) - 1;
          if (gapSize > tempJokers.size()) {
            if (potentialRun.size() == 2 && !tempJokers.isEmpty()) {
              potentialRun.add(tempJokers.remove(0));
              actualRuns.add(potentialRun);
            } else if (potentialRun.size() > 2) {
              actualRuns.add(potentialRun);
            }
            potentialRun = new TreeSet<>();
            tempJokers = new ArrayList<>(jokers);
          } else {
            for (int i = 0; i < gapSize; i++) {
              potentialRun.add(tempJokers.remove(0));
            }
          }
        }
        potentialRun.add(card);
        lastCard = card;
      }
      if (potentialRun.size() == 2 && !tempJokers.isEmpty()) {
        potentialRun.add(tempJokers.remove(0));
        actualRuns.add(potentialRun);
      } else if (potentialRun.size() > 2) {
        actualRuns.add(potentialRun);
      }
    }
    return actualRuns;
  }

  static private String getRunGapErrorDescription(Card previous, Card current, List<Card> jokerValues) {
    StringBuilder sb = new StringBuilder("gap found in run. ");
    sb.append("the previous card in the run was the ");
    sb.append(previous.toString());
    sb.append(". the current card is the ");
    sb.append(current.toString());
    sb.append(". this is a gap of ");
    sb.append(current.getValue().ordinal() - previous.getValue().ordinal());
    sb.append(" cards. ");
    if (jokerValues.isEmpty()) {
      sb.append(" you didn't have any jokers to fill this gap so this is not a valid run.");
    } else {
      if (jokerValues.size() == 1) {
        sb.append("Your joker is already being used to represent ");
      } else {
        sb.append("Your jokers are already being used to represent ");
      }
      for (Card card : jokerValues) {
        sb.append("the ");
        sb.append(card.toString());
      }
      sb.append(" and thus could not be used to fill this gap.");
    }

    return sb.toString();
  }

  // Accepts a list of cards and returns a boolean value indicating whether or not they are a run.
  // TODO: update this method to accept a "jokerPreference", which defines what the jokers should
  //       be in the case of ambiguity.
  static SequenceDetectionResult detectRun(List<Card> cards) {
    if (cards == null) {
      return new SequenceDetectionResult(CardSequenceType.RUN, false, new ArrayList<Card>(),
          "Cards was null.");
    }

    if (cards.size() < 3) {
      return new SequenceDetectionResult(CardSequenceType.RUN, false, new ArrayList<Card>(),
          "a run must be at least three in length");
    }

    List<Card> jokers = new ArrayList<Card>();
    List<Card> sortableCards = new ArrayList<Card>();
    for (Card card : cards) {
      if (card.getValue() == Value.UNKNOWN) {
        throw new IllegalArgumentException(
            "Detecting runs is not defined for lists that contain unknown cards.");
      }
      if (card.getValue() == Value.JOKER) {
        jokers.add(card);
      } else {
        sortableCards.add(card);
      }
    }

    List<Card> possibleJokerValues = new ArrayList<>();
    Collections.sort(sortableCards);
    Card previousCard = sortableCards.get(0);
    for (int i = 1; i < sortableCards.size(); i++) {
      Card currentCard = sortableCards.get(i);
      if (previousCard.getSuit() != currentCard.getSuit()) {
        return new SequenceDetectionResult(CardSequenceType.RUN, false, new ArrayList<Card>(),
            "suit mis-match");
      }
      int diff = currentCard.getValue().ordinal() - previousCard.getValue().ordinal();
      for (int j = 1; j < diff; j++) {
        if (jokers.size() > 0) {
          jokers.remove(0);
          Card newJokerValue = new Card(Value.values()[previousCard.getValue().ordinal() + j], previousCard.getSuit());
          possibleJokerValues.add(newJokerValue);
        } else {
          return new SequenceDetectionResult(CardSequenceType.RUN, false, new ArrayList<Card>(),
              getRunGapErrorDescription(currentCard, previousCard, possibleJokerValues));
        }
      }
      previousCard = currentCard;
    }

    // If jokers still has some jokers, there weren't gaps in the middle that constrained what the
    // jokers could represent so they'll be able to be on either end.
    Card firstCard = sortableCards.get(0);
    Card lastCard = sortableCards.get(sortableCards.size() - 1);
    while(!jokers.isEmpty()) {

      if (firstCard.getValue() != Value.ACE) {
        firstCard = new Card(Value.values()[firstCard.getValue().ordinal() - 1], firstCard.getSuit());
        possibleJokerValues.add(firstCard);
      }

      if (lastCard.getValue() != Value.KING) {
        lastCard = new Card(Value.values()[lastCard.getValue().ordinal() + 1], lastCard.getSuit());
        possibleJokerValues.add(lastCard);
      }
      jokers.remove(0);
    }
    return new SequenceDetectionResult(CardSequenceType.RUN, true, possibleJokerValues);
  }

  // TODO: have this support the unknown card.
  static List<Card> getPossiblePickups(Discard discard) {
    // Look at what new runs and multiples the possible pickups would give me.
    SortedSet<Card> values = new TreeSet<>(discard.getCards());
    values.addAll(discard.getJokerValues());
    values.removeAll(Card.JOKERS);

    List<Card> possiblePickUpCards = new ArrayList<>();

    Card firstCard = values.first();
    if (!discard.getJokerValues().contains(firstCard) && firstCard.getValue() != Value.UNKNOWN) {
      possiblePickUpCards.add(firstCard);
    }
    Card lastCard = values.last();
    if (!discard.getJokerValues().contains(lastCard) && !possiblePickUpCards.contains(lastCard) &&
        lastCard.getValue() != Value.UNKNOWN) {
      possiblePickUpCards.add(lastCard);
    }

    return possiblePickUpCards;
  }

  static List<SortedSet<Card>> getAllPossibleDiscards(SortedSet<Card> hand) {
    List<SortedSet<Card>> possibleDiscards = new ArrayList<>();
    possibleDiscards.addAll(CommonUtils.detectMultiples(hand));
    possibleDiscards.addAll(CommonUtils.detectRuns(hand));
    possibleDiscards.addAll(CommonUtils.getSingles(hand));
    return possibleDiscards;
  }

  static Map<Integer, List<SortedSet<Card>>> getApplicablePickupValues(List<SortedSet<Card>> discards, int distanceFromGoal) {
    Map<Integer, List<SortedSet<Card>>> result = new HashMap<>();
    for (SortedSet<Card> discard : discards) {
      int pickupValue = distanceFromGoal + Card.getValueOfCards(discard);
      if (pickupValue <= 10 && pickupValue >= 0) {
        if (!result.containsKey(pickupValue)) {
          result.put(pickupValue, new ArrayList<>());
        }
        result.get(pickupValue).add(discard);
      }
    }
    return result;
  }
}
