import java.util.Set;

abstract class Strategy extends Player {
  abstract int estimateReturnOnStrategy(Set<Card> hand);
  abstract void setParent(KillerPlayer parent);
}