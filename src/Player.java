import java.util.Map;
import java.util.Set;

abstract class Player implements GameEventListener {
  String name;
  Map<String, Integer> playerScoreView;
  abstract GameEvent takeTurn(Set<Card> hand, Discard lastDiscard);

  public abstract void onGameEvent(GameEvent event);

  String getName() {
    return name;
  }

  void setPlayerScoreView(Map<String, Integer> playerScoreView) {
    this.playerScoreView = playerScoreView;
  }
}
