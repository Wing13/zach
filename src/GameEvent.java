import java.util.List;
import java.util.Map;
import java.util.Set;

final class GameEvent {

  enum EventType {
    CALLED_ZACH,
    DISCARDED,
    GAME_START,
    ROUND_START,
    DECK_REFRESH,
    ROUND_END,
    GAME_END,
  }

  private final EventType type;
  private final Discard discard;
  private final Discard initialDiscard;
  private final Card pickedUp;
  private final String player;
  private final String startingPlayer;
  private final String winningPlayer;
  private final List<String> players;
  private final Map<String, Integer> roundScores;
  private final Map<String, Integer> gameScores;

  // getType is always valid and will always be defined.
  EventType getType() {
    return type;
  }

  // getDiscard will only be defined in the case that the EventType is DISCARDED.
  Discard getDiscard() {
    return discard;
  }

  // getPickedUp will only be defined in the case that the EventType is DISCARDED.
  Card getPickedUp() {
    return pickedUp;
  }

  // getPlayer will only be defined in the case that the EventType is DISCARDED.
  String getPlayer() {
    if (type == EventType.DISCARDED || type == EventType.CALLED_ZACH) {
      return player;
    } else {
      throw new UnsupportedOperationException("getPlayer is only defined for DISCARDED and " +
          "CALLED_ZACH events.");
    }
  }

  String getStartingPlayer() {
    if (type == EventType.ROUND_START) {
      return startingPlayer;
    } else {
      throw new UnsupportedOperationException("getStartingPlayer is only defined for ROUND_START " +
          "events.");
    }
  }

  Discard getInitialDiscard() {
    if (type == EventType.ROUND_START) {
      return initialDiscard;
    } else {
      throw new UnsupportedOperationException("getInitialDiscard is only defined for ROUND_START " +
          "events.");
    }
  }

  String getWinningPlayer() {
    if (type == EventType.ROUND_END || type == EventType.GAME_END) {
      return winningPlayer;
    } else {
      throw new UnsupportedOperationException("getWinningPlayer is only defined for ROUND_END " +
          "and GAME_END events.");
    }
  }

  Map<String, Integer> getRoundScores() {
    if (type == EventType.ROUND_END) {
      return roundScores;
    } else {
      throw new UnsupportedOperationException("getRoundScores is only defined for ROUND_END " +
          "events.");
    }
  }

  Map<String, Integer> getGameScores() {
    if (type == EventType.ROUND_END) {
      return gameScores;
    } else {
      throw new UnsupportedOperationException("getGameScores is only defined for ROUND_END " +
          "and GAME_END events.");
    }
  }

  List<String> getPlayers() {
    if (type == EventType.GAME_START) {
      return players;
    } else {
      throw new UnsupportedOperationException("getPlayers is only defined for the GAME_START " +
          "event.");
    }
  }

  String prettyPrint() {
    switch (type) {
      case CALLED_ZACH:
        return printCalledZach();
      case DISCARDED:
        return printDiscarded();
    }
    return toString();
  }

  String printCalledZach() {
    return "Call Zach";
  }

  String printDiscarded() {
    StringBuilder sb = new StringBuilder();
    sb.append("Discard:\n");
    for (Card card : discard.getCards()) {
      sb.append("  " + card.toString() + "\n");
    }
    sb.append("\n");
    sb.append("PickUp:\n");
    if (pickedUp != null) {
      sb.append("  " + pickedUp.toString());
    } else {
      sb.append("UNKNOWN");
    }
    return sb.toString();
  }

  private GameEvent(EventType type, Discard discard, Discard initialDiscard, Card pickedUp, String player,
                    String startingPlayer, String winningPlayer, List<String> players,
                    Map<String, Integer> roundScores, Map<String, Integer> gameScores) {
    this.type = type;
    this.discard = discard;
    this.initialDiscard = initialDiscard;
    this.pickedUp = pickedUp;
    this.player = player;
    this.startingPlayer = startingPlayer;
    this.winningPlayer = winningPlayer;
    this.players = players;
    this.roundScores = roundScores;
    this.gameScores = gameScores;
  }

  static class Builder {
    private EventType type;
    private Discard discard;
    private Discard initialDiscard;
    private Card pickedUp;
    private String player;
    private String startingPlayer;
    private String winningPlayer;
    private List<String> players;
    private Map<String, Integer> roundScores;
    private Map<String, Integer> gameScores;

    public Builder() {
      type = null;
      discard = null;
      initialDiscard = null;
      pickedUp = new Card(Value.UNKNOWN, Suit.UNKNOWN);
      player = null;
      startingPlayer = null;
      winningPlayer = null;
      players = null;
      roundScores = null;
      gameScores = null;
    }

    Builder setType(EventType type) {
      this.type = type;
      return this;
    }

    Builder setDiscard(Discard discard) {
      this.discard = discard;
      return this;
    }

    Builder setInitialDiscard(Discard discard) {
      this.initialDiscard = discard;
      return this;
    }

    Builder setPickedUp(Card pickedUp) {
      this.pickedUp = pickedUp;
      return this;
    }

    @Deprecated
    Builder setPlayer(Player player) {
      this.player = player.getName();
      return this;
    }

    Builder setPlayer(String playerName) {
      this.player = playerName;
      return this;
    }

    Builder setStartingPlayer(String playerName) {
      this.startingPlayer = playerName;
      return this;
    }

    Builder setWinningPlayer(String playerName) {
      this.winningPlayer = playerName;
      return this;
    }

    Builder setPlayers(List<String> playerNames) {
      this.players = playerNames;
      return this;
    }

    Builder setRoundScores(Map<String, Integer> roundScores) {
      this.roundScores = roundScores;
      return this;
    }

    Builder setGameScores(Map<String, Integer> gameScores) {
      this.gameScores = gameScores;
      return this;
    }

    GameEvent build() {
      if (type == null) {
        throw new InvalidTurnException("Invalid turn. Need to call either discard or Zach.");
      }
      if (player == null && (type == EventType.DISCARDED || type == EventType.CALLED_ZACH)) {
        throw new InvalidTurnException("Invalid turn. Need to provide the player.");
      }
      if (type == EventType.DISCARDED && discard == null) {
        throw new InvalidTurnException("Invalid turn. Need to discard!");
      }
      return new GameEvent(type, discard, initialDiscard, pickedUp, player, startingPlayer,
          winningPlayer, players, roundScores, gameScores);
    }
  }
}
