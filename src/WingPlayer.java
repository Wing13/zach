import java.util.*;

class WingPlayer extends Player {
  private Map<String, Double> probablePlayerHandValues = new HashMap<>();
  private Deck cardsLeft;
  private boolean closeToZach = false;
  double averageValue;
  WingPlayer(String name) {
    this.name = name;
    this.cardsLeft = Deck.getFullDeck();
    this.averageValue = 5.66;
  }

  GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {
    double lowestHandValue = 50;
    cardsLeft.removeCards(hand);
    for (Double handValue : probablePlayerHandValues.values()) {
      if (handValue < 10) {
        closeToZach = true;
      }
      if (handValue < lowestHandValue) {
        lowestHandValue = handValue;
      }
    }
    // Short circuit the entire method if I'm going to call Zach
    if (Card.getValueOfCards(hand) < 8 && Card.getValueOfCards(hand) < lowestHandValue) {
      GameEvent.Builder builder = new GameEvent.Builder()
        .setType(GameEvent.EventType.CALLED_ZACH)
        .setPlayer(this);
      return builder.build();
    }
    NavigableSet<Card> handNoJokers = new TreeSet<>(hand);

    // The outcome of calling getAndRemoveJokers is that handNoJokers becomes a set of cards
    // with no jokers in it anymore and jokers holds all (if any) jokers that were in the hand
    Set<Card> jokers = getAndRemoveJokers(handNoJokers);

    // Check if I already have matching cards in my hand
    Map<Value, Integer> numMatched = new HashMap<>();
    Set<Card> same = getMatchesFromHand(handNoJokers, numMatched);

    // Check if I have a run in my hand
    Set<Card> run = getRunsFromHand(handNoJokers, jokers);

    // Check if a discard card matches a card in my hand
    // Could replace with hash function for value of card and set want on a collision, but meh.
    Card want = getWantFromDiscard(lastDiscard, run, handNoJokers, closeToZach);

    // Run validation and choice
    if (!run.isEmpty()) {
      Iterator<Card> runIterator = run.iterator();
      Suit runSuit = null;
      Suit currentSuit;
      Card firstCard;
      Card secondCard = runIterator.next();
      int runLength = 1;
      while (runIterator.hasNext()) {
        firstCard = secondCard;
        secondCard = runIterator.next();
        runLength++;
        currentSuit = secondCard.getSuit();
        if (firstCard.getValue().ordinal() != secondCard.getValue().ordinal()-1
          && secondCard.getValue() != Value.JOKER && firstCard.getValue() != Value.JOKER) {
          runIterator.remove();
          secondCard = firstCard;
          continue;
        }
        // Remove cards if not sequential here
        if (runSuit == null) {
          runSuit = currentSuit;
        } else if (runSuit != currentSuit && !(currentSuit == Suit.JOKER_1 || currentSuit == Suit.JOKER_2)) {
          if (runLength > 3) {
            // If we get here, we have already looked through a valid run
            // But have other cards that are not in that run
            // e.g. 3S, 4S, 5S, 6H, 7H
            break;
          } else {
            runLength = 1;
            runSuit = currentSuit;
          }
        }
      }
      runIterator = run.iterator();
      while (runIterator.hasNext()) {
        Card card = runIterator.next();
        Suit suit = card.getSuit();
        if (suit != runSuit && !(suit == Suit.JOKER_1 || suit == Suit.JOKER_2)) {
          runIterator.remove();
        }
      }
    }

    // If I've added 2 sets of matches, remove the one with fewer matches
    // If the last discard has a card I want to make a larger match, don't discard that match
    // Example: 2S, 2D, 3C, 3S, 3D and lastDiscard was a 3H
    Iterator<Card> sameIterator;
    if (!same.isEmpty()) {
      if (want != null) {
        for (sameIterator = same.iterator(); sameIterator.hasNext();) {
          Card card = sameIterator.next();
          if (card.getValue() == want.getValue()) {
            sameIterator.remove();
            numMatched.remove(want.getValue());
          }
        }
      }
      if (numMatched.size() > 1) {
        Value keepValue = null;
        int max = 0;
        for (Value value : numMatched.keySet()) {
          int valueOfMatch = (value.ordinal()+1)*numMatched.get(value);
          if (valueOfMatch > max) {
            keepValue = value;
            max = valueOfMatch;
          }
        }
        sameIterator = same.iterator();
        while (sameIterator.hasNext()) {
          Card card = sameIterator.next();
          if (card.getValue() != keepValue) {
            sameIterator.remove();
          }
        }
      }
    }

    Iterator<Card> runIterator;
    GameEvent.EventType type = GameEvent.EventType.DISCARDED;
    Discard.Builder discard = new Discard.Builder();
    if (run.size() > 2 && run.size() > same.size()) {
      runIterator = run.iterator();
      boolean hasJoker = false;
      Card joker = null;
      while (runIterator.hasNext()) {
        Card card = runIterator.next();
        if (hasJoker) {
          hasJoker = false;
          int index = Arrays.asList(Value.values()).indexOf(card.getValue());
          discard.addJoker(joker, new Card(Value.values()[index-1], card.getSuit()));
        }
        if (card.getValue() == Value.JOKER) {
          joker = card;
          hasJoker = true;
        } else {
          discard.addCard(card);
        }
      }
    } else if (!same.isEmpty()) {
      sameIterator = same.iterator();
      while (sameIterator.hasNext()) {
        discard.addCard(sameIterator.next());
      }
    } else {
      if (want == null) {
        discard.addCard(getBestDiscard(handNoJokers));
      } else {
        boolean added = false;
        Iterator<Card> handIterator = handNoJokers.descendingIterator();
        while (handIterator.hasNext()) {
          Card card = handIterator.next();
          if (card.getValue() != want.getValue()) {
            discard.addCard(card);
            added = true;
            break;
          }
        }
        if (!added) {
          want = null;
          discard.addCard(getBestDiscard(handNoJokers));
        }
      }
    }
    return new GameEvent.Builder()
      .setType(type)
      .setPlayer(this)
      .setPickedUp(want)
      .setDiscard(discard.build())
      .build();
  }

  private Card getBestDiscard(NavigableSet<Card> handNoJokers) {
    Card discardCard = null;
    boolean added = false;
    Iterator<Card> handIterator = handNoJokers.descendingIterator();
    int numThisCard = 0;
    int numLastCard = 4;
    Card firstCard;
    Card secondCard = handIterator.next();
    while (handIterator.hasNext()) {
      firstCard = secondCard;
      secondCard = handIterator.next();
      if (firstCard.getNumericValue() == secondCard.getNumericValue()) {
        for (Card cardCheck : cardsLeft.cards) {
          if (cardCheck.getValue() == secondCard.getValue()) {
            numThisCard++;
          }
        }
        if (numThisCard <= numLastCard) {
          added = true;
          discardCard = secondCard;
          numLastCard = numThisCard;
        }
        numThisCard = 0;
      }
    }
    if (!added) {
      discardCard = handNoJokers.last();
    }
    return discardCard;
  }

  public void onGameEvent(GameEvent event) {
    if (event.getType() == GameEvent.EventType.ROUND_START) {
      this.cardsLeft = Deck.getFullDeck();
      this.averageValue = 5.66;
      this.closeToZach = false;
      cardsLeft.removeCards(event.getInitialDiscard().getCards());
    } else if (event.getType() == GameEvent.EventType.DISCARDED) {
      cardsLeft.removeCards(event.getDiscard().getCards());
      double totalValue = 0;
      for (Card card : cardsLeft.cards) {
        totalValue += card.getNumericValue();
      }
      averageValue = totalValue/(double) cardsLeft.cards.size();
      String playerName = event.getPlayer();
      Double currentHandValue = probablePlayerHandValues.get(playerName);
      if (currentHandValue == null) {
        probablePlayerHandValues.put(playerName, (double) 30);
        currentHandValue = (double) 30;
      }
      Card pickedUp = event.getPickedUp();
      int discardValue = 0;
      for (Card card : event.getDiscard().getCards()) {
        discardValue += card.getNumericValue();
      }
      currentHandValue -= discardValue;
      if (pickedUp == null || pickedUp.getValue() == Value.UNKNOWN) {
        probablePlayerHandValues.put(playerName, currentHandValue + averageValue);
      } else {
        probablePlayerHandValues.put(playerName, currentHandValue + pickedUp.getNumericValue()*2);
      }
    }
  }

  private Card getWantFromDiscard(Discard lastDiscard, Set<Card> run, Set<Card> handNoJokers, boolean closeToZach) {
    Card want = null;
    int cardNum = 0;
    for (Card card : lastDiscard.getCards()) {
      if (cardNum == 0 || cardNum == lastDiscard.getCards().size()-1) {
//        if (card.getValue() == Value.JOKER) {
//          // If I have the card that represents the joker, grab it
//          for (Card joker : lastDiscard.getJokerValues()) {
//            for (Card card2 : hand) {
//              if (joker.getValue() == card2.getValue() && joker.getSuit() == card2.getSuit()) {
//                return joker;
//              }
//            }
//          }
//        }
        // Check if a discarded card will make a match
        for (Card card2 : handNoJokers) {
          if (card.getValue() == card2.getValue()) {
            if (want == null) {
              want = card;
            } else if (want.getNumericValue() < card.getNumericValue()) {
              want = card;
            } else {
              // This may be the worst code I've ever written
              // This case is: Discard = 5H, 5C
              // handNoJokers = 5S, 6C, 8S
              // I want the 5C to go with the 6C
              for (Card checkAgain : handNoJokers) {
                if (Math.abs(checkAgain.getValue().ordinal() - card.getValue().ordinal()) == 1) {
                  want = card;
                }
              }
            }
          }
        }
        // Check if a discarded card will make a run
        for (Card card2 : run) {
          if (card2.getSuit() == card.getSuit()) {
            if (Math.abs(card2.getValue().ordinal() - card.getValue().ordinal()) == 1) {
              want = card;
            }
          }
        }
      }
      cardNum++;
    }
    if (closeToZach && want != null && want.getNumericValue() > averageValue) {
      return null;
    }
    return want;
  }

  private Set<Card> getMatchesFromHand(Set<Card> handNoJokers, Map<Value, Integer> numMatched) {
    Iterator<Card> handIterator = handNoJokers.iterator();
    Set<Card> same = new TreeSet<>();
    Card firstCard;
    Card secondCard = handIterator.next();
    int count = 0;
    while (handIterator.hasNext()) {
      firstCard = secondCard;
      secondCard = handIterator.next();
      // Check for matches
      if (firstCard.getValue() == secondCard.getValue()) {
        if (numMatched.get(firstCard.getValue()) == null) {
          count = 0;
        }
        numMatched.put(firstCard.getValue(), count++);
        same.add(firstCard);
        same.add(secondCard);
      }
    }
    return same;
  }

  private Set<Card> getRunsFromHand(Set<Card> handNoJokers, Set<Card> jokers) {
    Set<Card> run = new LinkedHashSet<>();
    int numJokers = jokers.size();
    if (handNoJokers.size() + numJokers >= 3) {
      Iterator<Card> handIterator = handNoJokers.iterator();
      Card firstCard = null;
      Card secondCard = handIterator.next();
      while (handIterator.hasNext()) {
        firstCard = secondCard;
        secondCard = handIterator.next();
        // Check for potential runs
        // This does not yet account for a hand like: 2H, 2S, 3H, 4H, 5H
        if (firstCard.getSuit() == secondCard.getSuit()) {
          int diff = secondCard.getValue().ordinal() - firstCard.getValue().ordinal();
          if (diff <= 1 + numJokers) { // And run lst element is one less than this new guy
            run.add(firstCard);
            while (diff != 1) {
              Iterator<Card> jokerIterator = jokers.iterator();
              run.add(jokerIterator.next());
              jokerIterator.remove();
              numJokers--;
              diff--;
            }
            run.add(secondCard);
          } else if (run.size() < 3) {
            run.clear();
          }
        } else if (run.size() < 3 && firstCard.getValue() != secondCard.getValue()) {
          run.clear();
        }
      }
    }
    return run;
  }

  private Set<Card> getAndRemoveJokers(Set<Card> hand) {
    Set<Card> jokers = new TreeSet<>();
    Iterator<Card> handIterator = hand.iterator();
    while (handIterator.hasNext()) {
      Card card = handIterator.next();
      if (card.getValue() == Value.JOKER) {
        handIterator.remove();
        jokers.add(card);
      }
    }
    return jokers;
  }

}
