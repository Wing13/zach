import java.util.Iterator;
import java.util.Set;

class DummyPlayer extends Player {
  DummyPlayer(String name) {
    this.name = name;
  }

  GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {
    GameEvent.EventType type = GameEvent.EventType.DISCARDED;
    Discard.Builder discard = new Discard.Builder();
    if (Card.getValueOfCards(hand) < 7) {
      type = GameEvent.EventType.CALLED_ZACH;
    } else {
      Iterator<Card> it = hand.iterator();
      Card card = it.next();
      if (card.getValue() == Value.JOKER) {
        discard.addJoker(card, new Card(Value.THREE, Suit.CLUBS));
      } else {
        discard.addCard(card);
      }
    }
    return new GameEvent.Builder()
      .setType(type)
      .setPlayer(this)
      .setDiscard(discard.build())
      .build();
  }

  public void onGameEvent(GameEvent event) {}
}
