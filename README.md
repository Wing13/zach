# Zach Game

This project is an implementation of the game Zach (as we call it) or Yaniv as the rest of the world calls it. It provides a simple interface, [Player](/src/Player.java), for participating in the game.

## How to run it

The instructions below assume you've cloned the repo to your local machine.

### From the command line

```bash
javac src/*
cd src
java ZachGame
```

### IntelliJ

These are the steps for importing an running the project from IntelliJ (These steps were created using IntelliJ IDEA 2016.1.2 so things may differ if you are visiting this from the future):

1. Open IntelliJ. 
2. Import Project.
3. Select the directory you clone the repository into.
4. Select "Create project from existing sources" and hit "Next".
5. Hit "Next" until you see the dialog about selecting JDK version.
4. Select 1.8 from the list on the left side.
4. Hit "Next" and then finish.

Once the IntelliJ project has been created, you can highlight the main method in [ZachGame.java](/src/ZachGame.java), click the "Run" menu in the top bar, and click "Run...". After that point you should just be able to run by clicking the green play button in the top bar.

## How To Write your own player

If you're interested in adding your own player, first off, awesome! Second, you'll want to create a new class that extends the [`Player`](/src/Player.java) class. Once you're satisfied with your `Player`, you can give it a whirl by updating the main method in [ZachGame.java](/src/ZachGame.java) to add your new `Player` to the `List` of `Player`s that are passed into the `ZachGame` constructor.

## Tests

You'll notice that there are tests included as well. For documentation about how to run the tests, you can visit the [Testing page](tests/README.md).