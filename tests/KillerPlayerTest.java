import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Bertrand on 6/5/2016.
 */
public class KillerPlayerTest {

  @Test
  public void correctlyFindsMultiples() {
    SortedSet<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.SEVEN, Suit.SPADES));
    cards.add(new Card(Value.SEVEN, Suit.HEARTS));
    cards.add(new Card(Value.TWO, Suit.HEARTS));
    cards.add(new Card(Value.NINE, Suit.DIAMONDS));
    List<SortedSet<Card>> multiples = CommonUtils.detectMultiples(cards);
    assertThat(multiples, hasSize(1));
    assertThat(multiples.get(0), containsInAnyOrder(new Card(Value.SEVEN, Suit.CLUBS),
        new Card(Value.SEVEN, Suit.SPADES), new Card(Value.SEVEN, Suit.HEARTS)));
  }

  @Test
  public void correctlyFindsRun() {
    SortedSet<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.EIGHT, Suit.CLUBS));
    cards.add(new Card(Value.NINE, Suit.CLUBS));
    List<SortedSet<Card>> multiples = CommonUtils.detectRuns(cards);
    assertThat(multiples, hasSize(1));
    assertThat(multiples.get(0), containsInAnyOrder(new Card(Value.SEVEN, Suit.CLUBS),
        new Card(Value.EIGHT, Suit.CLUBS), new Card(Value.NINE, Suit.CLUBS)));
  }

  @Test
  public void correctlyFindsTwoRuns() {
    SortedSet<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.EIGHT, Suit.CLUBS));
    cards.add(new Card(Value.NINE, Suit.CLUBS));
    cards.add(new Card(Value.SEVEN, Suit.HEARTS));
    cards.add(new Card(Value.EIGHT, Suit.HEARTS));
    cards.add(new Card(Value.NINE, Suit.HEARTS));
    List<SortedSet<Card>> multiples = CommonUtils.detectRuns(cards);

    Set<Card> expectedRun1 = new TreeSet<>();
    expectedRun1.add(new Card(Value.SEVEN, Suit.CLUBS));
    expectedRun1.add(new Card(Value.EIGHT, Suit.CLUBS));
    expectedRun1.add(new Card(Value.NINE, Suit.CLUBS));

    Set<Card> expectedRun2 = new TreeSet<>();
    expectedRun2.add(new Card(Value.SEVEN, Suit.HEARTS));
    expectedRun2.add(new Card(Value.EIGHT, Suit.HEARTS));
    expectedRun2.add(new Card(Value.NINE, Suit.HEARTS));

    assertThat(multiples, hasSize(2));
    assertThat(multiples, containsInAnyOrder(expectedRun1, expectedRun2));
  }

  @Test
  public void correctlyFindsRunWithJokers() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.JOKER, Suit.JOKER_1));
    cards.add(new Card(Value.NINE, Suit.CLUBS));
    List<SortedSet<Card>> runs = CommonUtils.detectRuns(cards);

    Set<Card> expectedRun1 = new TreeSet<>();
    expectedRun1.add(new Card(Value.SEVEN, Suit.CLUBS));
    expectedRun1.add(new Card(Value.JOKER, Suit.JOKER_1));
    expectedRun1.add(new Card(Value.NINE, Suit.CLUBS));

    assertThat(runs, hasSize(1));
    assertThat(runs.get(0), containsInAnyOrder(new Card(Value.SEVEN, Suit.CLUBS),
        new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.NINE, Suit.CLUBS)));
  }

  @Test
  public void correctlyFindsTwoRunWithJokers() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.JOKER, Suit.JOKER_1));
    cards.add(new Card(Value.EIGHT, Suit.CLUBS));
    List<SortedSet<Card>> runs = CommonUtils.detectRuns(cards);

    assertThat(runs, hasSize(1));
    assertThat(runs.get(0), containsInAnyOrder(new Card(Value.SEVEN, Suit.CLUBS),
        new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.EIGHT, Suit.CLUBS)));
  }

  @Test
  public void correctlyFindsTwoTwoRunWithJokers() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.JOKER, Suit.JOKER_1));
    cards.add(new Card(Value.EIGHT, Suit.CLUBS));
    cards.add(new Card(Value.TEN, Suit.DIAMONDS));
    cards.add(new Card(Value.JACK, Suit.DIAMONDS));
    List<SortedSet<Card>> runs = CommonUtils.detectRuns(cards);

    Set<Card> expectedRun1 = new TreeSet<>();
    expectedRun1.add(new Card(Value.SEVEN, Suit.CLUBS));
    expectedRun1.add(new Card(Value.JOKER, Suit.JOKER_1));
    expectedRun1.add(new Card(Value.EIGHT, Suit.CLUBS));

    Set<Card> expectedRun2 = new TreeSet<>();
    expectedRun2.add(new Card(Value.TEN, Suit.DIAMONDS));
    expectedRun2.add(new Card(Value.JOKER, Suit.JOKER_1));
    expectedRun2.add(new Card(Value.JACK, Suit.DIAMONDS));

    assertThat(runs, hasSize(2));
    assertThat(runs, containsInAnyOrder(expectedRun1, expectedRun2));
  }
}
