import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QualityTest {

  private final int playsPerScore = 5000;

  /*
  private Map<Integer, Map<String, Integer>> runReturnOnStrategyExperiment(List<Strategy> strategy) throws Exception {
    List<Player> players = new ArrayList<>();

    List<Strategy> singleStrategy = new ArrayList<>();
    singleStrategy.add(new MinimizationStrategy());
    Player adversaryA = new KillerPlayer("Adversary A", singleStrategy);
    players.add(adversaryA);

    singleStrategy = new ArrayList<>();
    singleStrategy.add(new MinimizationStrategy());
    Player adversaryB = new KillerPlayer("Adversary B", singleStrategy);
    players.add(adversaryB);

    KillerPlayer hero = new KillerPlayer("Our Hero", strategy);
    players.add(hero);

    ZachGame game = new ZachGame(players, false);

    Map<Integer, Map<String, Integer>> scoreResults = new HashMap<>();
    for (int score = 0; score < 50; score++) {
      Map<String, Integer> scoreResult = new HashMap<>();
      scoreResult.put("score", 0);
      scoreResult.put("winCount", 0);
      scoreResult.put("winningHandTotal", 0);
      scoreResults.put(score, scoreResult);
      for (int i = 0; i < playsPerScore; i++) {
        adversaryA.score = 0;
        adversaryB.score = 0;
        hero.score = score;

        game.playRound();

        int returnOnStrategy = hero.score - score;
        scoreResult.put("score", scoreResult.get("score") + returnOnStrategy);

        if (hero.score == 0) {
          scoreResult.put("winCount", scoreResult.get("winCount") + 1);
          scoreResult.put("winningHandTotal",
              scoreResult.get("winningHandTotal") + Card.getValueOfCards(hero.getRoundStartHand()));
        }
      }
    }
    return scoreResults;
  }

  private void printExperimentResults(Map<Integer, Map<String, Integer>> results) {
    System.out.println("Starting Score | Average ROS");
    for(Map.Entry<Integer, Map<String, Integer>> scoreSuccess : results.entrySet()) {
      System.out.println(String.format("%d | %f", scoreSuccess.getKey(),
          (double) scoreSuccess.getValue().get("score") / playsPerScore));
    }
  }

  private void printHitFiftyExperimentResults(Map<Integer, Map<String, Integer>> results) {
    System.out.println("Starting Score | Average ROS | Average Winning Hand Value | Success Percentage");
    for(Map.Entry<Integer, Map<String, Integer>> scoreSuccess : results.entrySet()) {
      System.out.println(String.format("%d | %f | %f | %f", scoreSuccess.getKey(),
          (double) scoreSuccess.getValue().get("score") / playsPerScore,
          (double) scoreSuccess.getValue().get("winningHandTotal") / scoreSuccess.getValue().get("winCount"),
          (double) scoreSuccess.getValue().get("winCount") / playsPerScore));
    }
  }

  @Test
  public void measureHitFiftySuccessRate() throws Exception {
    List<Strategy> strats = new ArrayList<>();
    strats.add(new HitFiftyStrategy());
    Map<Integer, Map<String, Integer>> scoreResults = runReturnOnStrategyExperiment(strats);
    printHitFiftyExperimentResults(scoreResults);
  }

  @Test
  public void measureMinimizationSuccessRate() throws Exception {
    List<Strategy> strats = new ArrayList<>();
    strats.add(new MinimizationStrategy());
    Map<Integer, Map<String, Integer>> scoreResults = runReturnOnStrategyExperiment(strats);
    printExperimentResults(scoreResults);
 }

  @Test
  public void measureOverallReturnOnStrategy() throws Exception {
    List<Strategy> strats = new ArrayList<>();
    strats.add(new MinimizationStrategy());
    strats.add(new HitFiftyStrategy());
    Map<Integer, Map<String, Integer>> scoreResults = runReturnOnStrategyExperiment(strats);
    printExperimentResults(scoreResults);
  }
*/
  @Test
  public void bestPlayerTest() throws Exception {
    String monsterName = "Killer's Monster";
    String monsterName2 = "Killer's Monster Too";
    String chimeraName = "Killer's Chimera";

    Map<String, Integer> wins = new HashMap<>();
    wins.put(chimeraName, 0);
    wins.put(monsterName, 0);
    //wins.put(monsterName2, 0);

    for (int i = 0; i < 1000; i++) {
      List<Player> players = new ArrayList<>();

      List<Strategy> strategies = new ArrayList<>();
      strategies.add(new MinimizationStrategy(3));
      //strategies.add(new HitFiftyStrategy());
      Player chimera = new KillerPlayer(chimeraName, strategies);
      players.add(chimera);

      List<Strategy> singleMinimizationStrategy = new ArrayList<>();
      singleMinimizationStrategy.add(new MinimizationStrategy(0));
      Player monster = new KillerPlayer(monsterName, singleMinimizationStrategy);
      players.add(monster);

      singleMinimizationStrategy = new ArrayList<>();
      singleMinimizationStrategy.add(new MinimizationStrategy(0));
      Player monster2 = new KillerPlayer(monsterName2, singleMinimizationStrategy);
      //players.add(monster2);

      ZachGame game = new ZachGame(players);
      //game.playGame();
      //wins.put(game.gameWinner.getName(), wins.get(game.gameWinner.getName()) + 1);
    }

    System.out.println("These were the standings when all was said and done:");
    for (Map.Entry entry : wins.entrySet()) {
      System.out.println("  " + entry.getKey() + ": " + entry.getValue().toString());
    }
  }

}
