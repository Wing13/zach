import java.util.Iterator;
import java.util.TreeSet;
import java.util.Set;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CardTest {

  private boolean compareValuesInOrder(Set<Card> expected, Set<Card> actual) {
    if (expected.size() != actual.size()) {
      return false;
    }
    Iterator<Card> expectedIterator = expected.iterator();
    for (Card actualCard : actual) {
      Card expectedCard = expectedIterator.next();
      if (expectedCard.getValue() != actualCard.getValue() ||
        expectedCard.getSuit() != actualCard.getSuit()) {
      return false;
      }
    }
    return true;
  }

  @Test
  public void sortCards() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.FIVE, Suit.DIAMONDS));
    cards.add(new Card(Value.SEVEN, Suit.HEARTS));
    cards.add(new Card(Value.THREE, Suit.CLUBS));

    Set<Card> expected = new TreeSet<>();
    expected.add(new Card(Value.THREE, Suit.CLUBS));
    expected.add(new Card(Value.FIVE, Suit.DIAMONDS));
    expected.add(new Card(Value.SEVEN, Suit.HEARTS));

    assertEquals(cards, expected);
  }

  @Test
  public void testCompareValuesInOrder() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.FIVE, Suit.DIAMONDS));
    cards.add(new Card(Value.SEVEN, Suit.HEARTS));
    cards.add(new Card(Value.THREE, Suit.CLUBS));

    assertTrue(compareValuesInOrder(cards, cards));
  }

  @Test
  public void sortCardsMultipleValues() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.FIVE, Suit.DIAMONDS));
    cards.add(new Card(Value.SEVEN, Suit.HEARTS));
    cards.add(new Card(Value.SEVEN, Suit.CLUBS));
    cards.add(new Card(Value.THREE, Suit.CLUBS));

    Set<Card> expected = new TreeSet<>();
    expected.add(new Card(Value.THREE, Suit.CLUBS));
    expected.add(new Card(Value.FIVE, Suit.DIAMONDS));
    expected.add(new Card(Value.SEVEN, Suit.HEARTS));
    expected.add(new Card(Value.SEVEN, Suit.CLUBS));

    boolean firstOrdering = compareValuesInOrder(expected, cards);

    Set<Card> expectedSuitsSwapped = new TreeSet<>();
    expectedSuitsSwapped.add(new Card(Value.THREE, Suit.CLUBS));
    expectedSuitsSwapped.add(new Card(Value.FIVE, Suit.DIAMONDS));
    expectedSuitsSwapped.add(new Card(Value.SEVEN, Suit.CLUBS));
    expectedSuitsSwapped.add(new Card(Value.SEVEN, Suit.HEARTS));

    boolean secondOrdering = compareValuesInOrder(expectedSuitsSwapped, cards);

    assertTrue(firstOrdering || secondOrdering);
  }

  @Test
  public void sortCardsByCardOrdering() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.QUEEN, Suit.DIAMONDS));
    cards.add(new Card(Value.SEVEN, Suit.HEARTS));
    cards.add(new Card(Value.KING, Suit.CLUBS));
    cards.add(new Card(Value.FIVE, Suit.CLUBS));

    Set<Card> expected = new TreeSet<>();
    expected.add(new Card(Value.FIVE, Suit.CLUBS));
    expected.add(new Card(Value.SEVEN, Suit.HEARTS));
    expected.add(new Card(Value.QUEEN, Suit.DIAMONDS));
    expected.add(new Card(Value.KING, Suit.CLUBS));

    assertTrue(compareValuesInOrder(expected, cards));
  }

  @Test(expected = IllegalArgumentException.class)
  public void rejectCreationOfUnknownValueWithKnownSuit() {
    new Card(Value.UNKNOWN, Suit.SPADES);
  }

  @Test(expected = IllegalArgumentException.class)
  public void rejectCreationOfKnownValueWithUnknownSuit() {
    new Card(Value.FOUR, Suit.UNKNOWN);
  }

  @Test(expected = IllegalArgumentException.class)
  public void rejectCreationOfOffSuitJokerCards() {
    new Card(Value.JOKER, Suit.SPADES);
  }

  @Test(expected = IllegalArgumentException.class)
  public void rejectCreationOfJokerSuitStandardCards() {
    new Card(Value.SEVEN, Suit.JOKER_1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void rejectCreationOfJokerSuitStandardCards2() {
    new Card(Value.JACK, Suit.JOKER_2);
  }

  @Test
  public void acceptCreationOfUnknownCard() {
    Card card = new Card(Value.UNKNOWN, Suit.UNKNOWN);
    assertTrue(card.getValue() == Value.UNKNOWN);
    assertTrue(card.getSuit() == Suit.UNKNOWN);
  }

  @Test
  public void acceptCreationOfJokers() {
    Card card = new Card(Value.JOKER, Suit.JOKER_1);
    assertTrue(card.getValue() == Value.JOKER);
    assertTrue(card.getSuit() == Suit.JOKER_1);

    card = new Card(Value.JOKER, Suit.JOKER_2);
    assertTrue(card.getValue() == Value.JOKER);
    assertTrue(card.getSuit() == Suit.JOKER_2);
  }

  @Test
  public void acceptCreationOfStandardCards() {
    Card card = new Card(Value.FOUR, Suit.DIAMONDS);
    assertTrue(card.getValue() == Value.FOUR);
    assertTrue(card.getSuit() == Suit.DIAMONDS);

    card = new Card(Value.SEVEN, Suit.HEARTS);
    assertTrue(card.getValue() == Value.SEVEN);
    assertTrue(card.getSuit() == Suit.HEARTS);
  }

  @Test
  public void detectQuadRun() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.TWO, Suit.DIAMONDS));
    cards.add(new Card(Value.THREE, Suit.DIAMONDS));
    cards.add(new Card(Value.FOUR, Suit.DIAMONDS));
    cards.add(new Card(Value.FIVE, Suit.DIAMONDS));
    assertTrue(Card.isRun(cards));
  }

  @Test
  public void detectTripleRun() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.TEN, Suit.HEARTS));
    cards.add(new Card(Value.JACK, Suit.HEARTS));
    cards.add(new Card(Value.QUEEN, Suit.HEARTS));
    assertTrue(Card.isRun(cards));
  }

  @Test
  public void detectVanillaRun() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.TWO, Suit.CLUBS));
    cards.add(new Card(Value.THREE, Suit.CLUBS));
    cards.add(new Card(Value.FOUR, Suit.CLUBS));
    cards.add(new Card(Value.FIVE, Suit.CLUBS));
    cards.add(new Card(Value.SIX, Suit.CLUBS));
    assertTrue(Card.isRun(cards));
  }

  @Test
  public void rejectNotARun() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.TWO, Suit.CLUBS));
    cards.add(new Card(Value.FOUR, Suit.CLUBS));
    cards.add(new Card(Value.FIVE, Suit.CLUBS));
    cards.add(new Card(Value.SIX, Suit.CLUBS));
    assertFalse(Card.isRun(cards));
  }

  @Test
  public void rejectShortRun() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.TWO, Suit.CLUBS));
    assertFalse(Card.isRun(cards));
  }

  @Test
  public void acceptPair() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.ACE, Suit.DIAMONDS));
    assertTrue(Card.isMultiple(cards));
  }

  @Test
  public void acceptTriple() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.ACE, Suit.DIAMONDS));
    cards.add(new Card(Value.ACE, Suit.HEARTS));
    assertTrue(Card.isMultiple(cards));
  }

  @Test
  public void acceptQuad() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.ACE, Suit.DIAMONDS));
    cards.add(new Card(Value.ACE, Suit.HEARTS));
    cards.add(new Card(Value.ACE, Suit.SPADES));
    assertTrue(Card.isMultiple(cards));
  }


  @Test
  public void rejectNotAMultiple() {
    Set<Card> cards = new TreeSet<>();
    cards.add(new Card(Value.ACE, Suit.CLUBS));
    cards.add(new Card(Value.TWO, Suit.CLUBS));
    cards.add(new Card(Value.FOUR, Suit.CLUBS));
    cards.add(new Card(Value.FIVE, Suit.CLUBS));
    cards.add(new Card(Value.SIX, Suit.CLUBS));
    assertFalse(Card.isMultiple(cards));
  }
}