import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

public class GameEventTest {

  private GameEvent.Builder builder;
  @Before
  public void setup(){
  this.builder = new GameEvent.Builder();
  }

  @Test
  public void disallowMoreThanThreeJokers() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.JOKER, Suit.JOKER_1));
    discard.add(new Card(Value.JOKER, Suit.JOKER_2));
    discard.add(new Card(Value.JOKER, Suit.JOKER_1));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowMoreThanFiveCards() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.TWO, Suit.CLUBS));
    discard.add(new Card(Value.THREE, Suit.CLUBS));
    discard.add(new Card(Value.FOUR, Suit.CLUBS));
    discard.add(new Card(Value.FIVE, Suit.CLUBS));
    discard.add(new Card(Value.SIX, Suit.CLUBS));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void allowFiveCardRun() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.TWO, Suit.CLUBS));
    discard.add(new Card(Value.THREE, Suit.CLUBS));
    discard.add(new Card(Value.FOUR, Suit.CLUBS));
    discard.add(new Card(Value.FIVE, Suit.CLUBS));
  //  assertTrue(this.builder.isValidDiscard(discard));
  }

  @Test
  public void allowFiveCardRunWithJokers() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.JOKER, Suit.JOKER_1));
    discard.add(new Card(Value.THREE, Suit.CLUBS));
    discard.add(new Card(Value.JOKER, Suit.JOKER_2));
    discard.add(new Card(Value.FIVE, Suit.CLUBS));
  //  assertTrue(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowDuplicateCards() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.ACE, Suit.CLUBS));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void allowPairs() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.ACE, Suit.DIAMONDS));
  //  assertTrue(this.builder.isValidDiscard(discard));
  }

  @Test
  public void allowTriples() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.ACE, Suit.DIAMONDS));
    discard.add(new Card(Value.ACE, Suit.SPADES));
  //  assertTrue(this.builder.isValidDiscard(discard));
  }

  @Test
  public void allowQuads() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.ACE, Suit.DIAMONDS));
    discard.add(new Card(Value.ACE, Suit.SPADES));
    discard.add(new Card(Value.ACE, Suit.HEARTS));
  //  assertTrue(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowNoDiscard() {
    Set<Card> discard = null;
  //  assertFalse(this.builder.isValidDiscard(discard));
    discard = new TreeSet<>();
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void allowOneCardDiscard() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.SPADES));
  //  assertTrue(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowNotPairOrRun() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.JACK, Suit.DIAMONDS));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowRunNonMatchingSuit() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.TWO, Suit.DIAMONDS));
    discard.add(new Card(Value.THREE, Suit.DIAMONDS));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowRunNonSequential() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.DIAMONDS));
    discard.add(new Card(Value.TWO, Suit.DIAMONDS));
    discard.add(new Card(Value.FOUR, Suit.DIAMONDS));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowTwoPair() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.DIAMONDS));
    discard.add(new Card(Value.ACE, Suit.SPADES));
    discard.add(new Card(Value.TWO, Suit.DIAMONDS));
    discard.add(new Card(Value.TWO, Suit.SPADES));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }

  @Test
  public void disallowPairAndRun() {
    Set<Card> discard = new TreeSet<>();
    discard.add(new Card(Value.ACE, Suit.CLUBS));
    discard.add(new Card(Value.ACE, Suit.DIAMONDS));
    discard.add(new Card(Value.TWO, Suit.DIAMONDS));
    discard.add(new Card(Value.THREE, Suit.DIAMONDS));
  //  assertFalse(this.builder.isValidDiscard(discard));
  }
}
