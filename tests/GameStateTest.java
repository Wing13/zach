import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class GameStateTest {

  @Test(expected = InvalidTurnException.class)
  public void disallowPlayersFromDiscardingCardsTheyDontHaveInTheirHand() {
    // create a "nefarious" Player that attempts to discard cards not in its hand.
    Player nefariousPlayer = new DummyPlayer("Nef R. Ious") {
      @Override
      public GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {

        // Initialize the GameEvent Builder.
        GameEvent.Builder builder = new GameEvent.Builder().setPlayer(this);
        builder.setType(GameEvent.EventType.DISCARDED);

        // Get a deck and remove the cards in my hand from it so that I'm sure that I'm discarding
        // a card that's not in my hand.
        Deck deck = Deck.getFullDeck();
        deck.removeCards(hand);

        // also remove jokers because they're special.
        Set<Card> jokers = new TreeSet<>();
        jokers.add(new Card(Value.JOKER, Suit.JOKER_1));
        jokers.add(new Card(Value.JOKER, Suit.JOKER_2));
        deck.removeCards(jokers);

        Card nonHandCard = deck.drawRandomCard();

        // Create the discard.
        Discard.Builder discard = new Discard.Builder();
        discard.addCard(nonHandCard);
        builder.setDiscard(discard.build());
        return builder.build();
      }
      public void onGameEvent(GameEvent event) {}
    };

    List<Player> players = new ArrayList<>();
    players.add(nefariousPlayer);

    GameState state = new GameState(Deck.getFullDeck());

    state.deal(players);
    GameEvent event = nefariousPlayer.takeTurn(state.getCardsInPlayersHand(nefariousPlayer),
        new Discard.Builder().addCard(new Card(Value.ACE, Suit.CLUBS)).build());

    state.onGameEvent(event);
  }

  @Test(expected = InvalidTurnException.class)
  public void disallowPlayersFromDrawingCardsThatAreNotInTheMostRecentDiscard() {

    // create a "nefarious" Player that attempts to draw cards from the discard that aren't in the
    // most recent discard pile.
    Player nefariousPlayer = new DummyPlayer("Nef R. Ious") {

      @Override
      public GameEvent takeTurn(Set<Card> hand, Discard lastDiscard) {

        // Initialize the GameEvent Builder.
        GameEvent.Builder builder = new GameEvent.Builder().setPlayer(this);
        builder.setType(GameEvent.EventType.DISCARDED);

        // Create the discard.
        Discard.Builder discard = new Discard.Builder();
        Iterator<Card> it = hand.iterator();
        Card card = it.next();
        if (card.getValue() == Value.JOKER) {
          discard.addJoker(card, new Card(Value.SEVEN, Suit.CLUBS));
        } else {
          discard.addCard(it.next());
        }
        builder.setDiscard(discard.build());

        // Get a deck and remove the cards in the most recent discard from it so that I'm sure that
        // I'm picking up a card that's not in the most recent discard.
        Deck deck = Deck.getFullDeck();
        deck.removeCards(lastDiscard.getCards());
        Card nonDiscardCard = deck.drawRandomCard();
        builder.setPickedUp(nonDiscardCard);

        return builder.build();
      }

      public void onGameEvent(GameEvent event) {}
    };

    List<Player> players = new ArrayList<>();
    players.add(nefariousPlayer);

    GameState state = new GameState(Deck.getFullDeck());

    state.deal(players);
    GameEvent event = nefariousPlayer.takeTurn(state.getCardsInPlayersHand(nefariousPlayer),
        new Discard.Builder().addCard(new Card(Value.ACE, Suit.CLUBS)).build());

    state.onGameEvent(event);
  }

//  @Test(expected = Exception.class)
  public void disallowPlayersFromDrawingTheJokerIfTheyHaveNotDiscardedTheCardThatItRepresents() {

    // TODO: Implement this after the Discard class has been created
    Player player1 = new DummyPlayer("player_1");

    Set<Card> hand1 = new TreeSet<>();
    hand1.add(new Card(Value.ACE, Suit.CLUBS));
    hand1.add(new Card(Value.TWO, Suit.CLUBS));
    hand1.add(new Card(Value.THREE, Suit.CLUBS));
    hand1.add(new Card(Value.FOUR, Suit.CLUBS));
    hand1.add(new Card(Value.FIVE, Suit.CLUBS));

    Deck deck = Deck.getFullDeck();
    deck.removeCards(hand1);

    Map<String, Set<Card>> playersHands = new HashMap<>();

    playersHands.put(player1.getName(), hand1);

    Discard.Builder jokerDiscard = new Discard.Builder();

    jokerDiscard.addCard(new Card(Value.NINE, Suit.HEARTS));
    jokerDiscard.addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.TEN, Suit.HEARTS));
    jokerDiscard.addCard(new Card(Value.JACK, Suit.HEARTS));

    Discard finalDiscard = jokerDiscard.build();
    deck.removeCards(finalDiscard.getCards());

    List<Discard> discardPile = new ArrayList<>();
    discardPile.add(finalDiscard);

    GameState state = new GameState(deck, discardPile, playersHands);

    GameEvent.Builder builder = new GameEvent.Builder()
      .setPlayer(player1)
      .setType(GameEvent.EventType.DISCARDED);

    Iterator<Card> it = hand1.iterator();
    Discard discard = new Discard.Builder().addCard(it.next()).build();
    builder.setDiscard(discard);

    builder.setPickedUp(new Card(Value.JOKER, Suit.JOKER_1));

    state.onGameEvent(builder.build());
  }

  @Test
  public void allowDrawingFromTheSmallEnd() {
    Discard.Builder discard = new Discard.Builder();
    discard.addCard(new Card(Value.EIGHT, Suit.CLUBS));
    discard.addCard(new Card(Value.NINE, Suit.CLUBS));
    discard.addCard(new Card(Value.TEN, Suit.CLUBS));

    Card pickedUp = new Card(Value.EIGHT, Suit.CLUBS);
    GameState.playerDrewFromTheEdge(pickedUp, discard.build());
  }

  @Test
  public void allowDrawingFromTheLargeEnd() {
    Discard.Builder discard = new Discard.Builder();
    discard.addCard(new Card(Value.ACE, Suit.DIAMONDS));
    discard.addCard(new Card(Value.TWO, Suit.DIAMONDS));
    discard.addCard(new Card(Value.THREE, Suit.DIAMONDS));

    Card pickedUp = new Card(Value.THREE, Suit.DIAMONDS);
    assertTrue(GameState.playerDrewFromTheEdge(pickedUp, discard.build()));
  }

  @Test
  public void disallowDrawingFromTheMiddle() {
    Discard.Builder discard = new Discard.Builder();
    discard.addCard(new Card(Value.JACK, Suit.HEARTS));
    discard.addCard(new Card(Value.QUEEN, Suit.HEARTS));
    discard.addCard(new Card(Value.KING, Suit.HEARTS));

    Card pickedUp = new Card(Value.QUEEN, Suit.HEARTS);
    assertFalse(GameState.playerDrewFromTheEdge(pickedUp, discard.build()));
  }

  @Test
  public void allowDrawingFromTheMiddleOfATriple() {
    Discard.Builder discard = new Discard.Builder();
    discard.addCard(new Card(Value.JACK, Suit.HEARTS));
    discard.addCard(new Card(Value.JACK, Suit.DIAMONDS));
    discard.addCard(new Card(Value.JACK, Suit.CLUBS));

    Card pickedUp = new Card(Value.JACK, Suit.HEARTS);
    assertTrue(GameState.playerDrewFromTheEdge(pickedUp, discard.build()));
  }

}
