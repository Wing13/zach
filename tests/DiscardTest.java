import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.not;

public class DiscardTest {

  @Test(expected = IllegalArgumentException.class)
  public void rejectsJokersFromNormalAddCard() {
    Discard.Builder builder = new Discard.Builder();
    builder.addCard(new Card(Value.JOKER, Suit.JOKER_1));
  }

  @Test(expected = IllegalArgumentException.class)
  public void rejectsUnknownCardFromAddCard() {
    Discard.Builder builder = new Discard.Builder();
    builder.addCard(new Card(Value.UNKNOWN, Suit.UNKNOWN));
  }

  @Test
  public void acceptsNormalCardInAddCard() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.ACE, Suit.SPADES))
        .build();

    assertThat(discard.getCards(), contains(new Card(Value.ACE, Suit.SPADES)));
  }

  @Test
  public void acceptsJokerInAddJoker() {
    Discard discard = new Discard.Builder()
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.ACE, Suit.SPADES))
        .build();

    assertThat(discard.getCards(), contains(new Card(Value.JOKER, Suit.JOKER_1)));
    assertThat(discard.getJokerValues(), contains(new Card(Value.ACE, Suit.SPADES)));
  }

  @Test
  public void canRemoveCardFromDiscard() {
    Discard discard = new Discard.Builder()
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.ACE, Suit.SPADES))
        .build();

    assertThat(discard.getCards(), contains(new Card(Value.JOKER, Suit.JOKER_1)));
    assertThat(discard.getJokerValues(), contains(new Card(Value.ACE, Suit.SPADES)));

    Card card = discard.removeCard(new Card(Value.JOKER, Suit.JOKER_1));

    assertThat(discard.getCards(), not(contains(new Card(Value.JOKER, Suit.JOKER_1))));
  }

  @Test(expected = IllegalStateException.class)
  public void disallowRemovingMoreThanOneCardFromDiscard() {
    Discard discard = new Discard.Builder()
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.ACE, Suit.SPADES))
        .build();

    discard.removeCard(new Card(Value.JOKER, Suit.JOKER_1));
    discard.removeCard(new Card(Value.JOKER, Suit.JOKER_1));
  }

  @Test(expected = IllegalStateException.class)
  public void disallowNonRunNonMultipleDiscards() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.ACE, Suit.SPADES))
        .addCard(new Card(Value.TWO, Suit.SPADES))
        .addCard(new Card(Value.FIVE, Suit.SPADES))
        .build();
  }

  @Test
  public void acceptRunDiscards() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.FIVE, Suit.DIAMONDS))
        .addCard(new Card(Value.SIX, Suit.DIAMONDS))
        .addCard(new Card(Value.SEVEN, Suit.DIAMONDS))
        .build();

    assertThat(discard.getCards(), containsInAnyOrder(new Card(Value.FIVE, Suit.DIAMONDS),
        new Card(Value.SIX, Suit.DIAMONDS), new Card(Value.SEVEN, Suit.DIAMONDS)));
  }

  @Test
  public void acceptRunWithJokerDiscards() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.FIVE, Suit.DIAMONDS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.SIX, Suit.DIAMONDS))
        .addCard(new Card(Value.SEVEN, Suit.DIAMONDS))
        .build();

    assertThat(discard.getCards(), containsInAnyOrder(new Card(Value.FIVE, Suit.DIAMONDS),
        new Card(Value.JOKER, Suit.JOKER_1),
        new Card(Value.SEVEN, Suit.DIAMONDS)));
  }

  @Test
  public void acceptRunWithTwoJokerDiscards() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.FIVE, Suit.DIAMONDS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.SIX, Suit.DIAMONDS))
        .addCard(new Card(Value.SEVEN, Suit.DIAMONDS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_2), new Card(Value.EIGHT, Suit.DIAMONDS))
        .addCard(new Card(Value.NINE, Suit.DIAMONDS))
        .build();

    assertThat(discard.getCards(), containsInAnyOrder(new Card(Value.FIVE, Suit.DIAMONDS),
        new Card(Value.JOKER, Suit.JOKER_1),
        new Card(Value.SEVEN, Suit.DIAMONDS),
        new Card(Value.JOKER, Suit.JOKER_2),
        new Card(Value.NINE, Suit.DIAMONDS)));
  }

  @Test
  public void acceptMultipleCards() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.FIVE, Suit.DIAMONDS))
        .addCard(new Card(Value.FIVE, Suit.SPADES))
        .addCard(new Card(Value.FIVE, Suit.HEARTS))
        .build();

    assertThat(discard.getCards(), containsInAnyOrder(new Card(Value.FIVE, Suit.DIAMONDS),
        new Card(Value.FIVE, Suit.SPADES),
        new Card(Value.FIVE, Suit.HEARTS)));
  }

  @Test
  public void acceptMultipleCardsWithJoker() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.FIVE, Suit.DIAMONDS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_2), new Card(Value.FIVE, Suit.SPADES))
        .addCard(new Card(Value.FIVE, Suit.HEARTS))
        .build();

    assertThat(discard.getCards(), containsInAnyOrder(new Card(Value.FIVE, Suit.DIAMONDS),
        new Card(Value.JOKER, Suit.JOKER_2),
        new Card(Value.FIVE, Suit.HEARTS)));
  }

  @Test
  public void acceptMultipleCardsWithTwoJokers() {
    Discard discard = new Discard.Builder()
        .addCard(new Card(Value.SIX, Suit.DIAMONDS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_2), new Card(Value.SIX, Suit.SPADES))
        .addCard(new Card(Value.SIX, Suit.HEARTS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.SIX, Suit.CLUBS))
        .build();

    assertThat(discard.getCards(), containsInAnyOrder(new Card(Value.SIX, Suit.DIAMONDS),
        new Card(Value.JOKER, Suit.JOKER_2),
        new Card(Value.SIX, Suit.HEARTS),
        new Card(Value.JOKER, Suit.JOKER_1)));
  }

  @Test(expected = IllegalStateException.class)
  public void disallowTwoJokersRepresentingTheSameValue() {
    new Discard.Builder()
        .addCard(new Card(Value.SIX, Suit.DIAMONDS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_2), new Card(Value.SIX, Suit.SPADES))
        .addCard(new Card(Value.SIX, Suit.HEARTS))
        .addJoker(new Card(Value.JOKER, Suit.JOKER_1), new Card(Value.SIX, Suit.SPADES))
        .build();
  }
}
