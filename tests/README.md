# Zach Tests

This directory is dedicated to testing the Zach classes.

## How do I run Tests?

I set this up in IntelliJ so that's what I'll describe here but I don't think there should be any problems with setting it up in any other IDE like Eclipse or another one. I'm not sure how these tests can be run directly from the commandline. If you figure out another way to run the tests feel free to update this file.

### IntelliJ IDEA

First you need to add a dependency on JUnit. The easiest way to do that is as follows:

1. Open up one of the test class files.
2. Click on one of the junit imports which should be underlined in red.
3. Wait until the little red light bulb shows up.
4. Click the lightbulb and select "Add 'JUnit4' to class path"
5. Select "Copy 'JUnit4' library files to".
6. Click "OK".

All of the references should be resolved. Now, we need to create a new Run configuration that executes the tests:

1. In the code outliner on the left side right click on the project name ("zach" most likely).
2. Select "Open Module Settings" from the drop down menu.
3. You should see the project directories there. Right click the "tests" directory.
4. Select "Tests" from the dropdown menu.
5. Click "OK".
6. Right click the project name again.
7. Select "Create 'All Tests'...".
8. All the defaults in this dialog should be good. Click "OK".

Now, in the upper right hand corner you should see that it says "All in zach" instead of whatever it said before. Now when you click run, it'll run the tests instead of the ZachGame Main. In order to run the ZachGame again just click the dropdown where it says "All in zach" and select ZachGame. Now when you click run, it'll run the main code.

## How do I add Tests?

The convention should typically be, if you want to test the behavior of a particular class, add tests to <CLASS_NAME>Test.java or create that file and add them there, if it doesn't exist yet.
